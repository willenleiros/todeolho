package todeolho

import grails.transaction.Transactional

@Transactional
class UserService {

    def salvar(User userInstance, Role roleInstance) {

		try{
			
			User user = userInstance.save flush:true
			
			UserRole userRole = new UserRole()
			
			userRole.user = userInstance
			
			userRole.role = roleInstance
			
			userRole.save flush:true
			
			Perfil perfil = new Perfil()
			
			perfil.setUser(user)
			perfil.setNome(user.username)
			
			perfil.save flush:true
			
		}
		catch(RuntimeException e){
			println e.message
			throw new RuntimeException(e.message)	
		}
    }
}
