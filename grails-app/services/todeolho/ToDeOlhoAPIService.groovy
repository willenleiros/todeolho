package todeolho

import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class ToDeOlhoAPIService {
	
	static transactional = true
	
	def springSecurityService

    def String executarCurtida(Denuncia den){
		
		
		User usuario = springSecurityService.currentUser
		Perfil perfil = Perfil.findByUser(usuario)
		
		if(perfil == null){
			return false
		}
		
		println("Service perfil json: ")
		println(perfil as JSON )
		
		
		ToDeOlho toDeOlho = ToDeOlho.findByDenunciaAndPerfil(den,perfil)
		
		if(toDeOlho){
			toDeOlho.delete flush:true
			den.setTotalToDeOlho(den.totalToDeOlho-1)
			den.save flush:true
			return "Deletou toDeOlho"
		}
		else{
			toDeOlho = new ToDeOlho(denuncia:den,perfil:perfil)
			toDeOlho.save flush:true
			den.setTotalToDeOlho(den.totalToDeOlho+1)
			den.save flush:true
			return "Salvou toDeOlho"
		}
	}
}
