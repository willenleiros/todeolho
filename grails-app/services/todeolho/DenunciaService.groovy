package todeolho

import grails.transaction.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile

@Transactional
class DenunciaService {

	static transactional = true
	
	def hdImageService

	def save(Denuncia denuncia, Localizacao localizacao, Perfil perfil, byte[] imagem) throws Exception {
		
		println "Entrou no denuncia service"
		
		if(denuncia == null){
			throw new RuntimeException("denuncia nulo")
		}
		
		if(denuncia.descricao == null){
			throw new RuntimeException("É necessário inserir uma descrição.")
		}
		
		if(imagem == null){
			throw new RuntimeException("imagem nulo")
		}
		
		if(perfil == null){
			throw new RuntimeException("perfil nulo")
		}
		
		if(localizacao == null){
			throw new RuntimeException("localizacao nulo")
		}
		
		if(!localizacao.validate()){
			throw new RuntimeException("É necessario inserir uma localização.")
		}
		
		try{
			Localizacao loc = localizacao.save flush:true
			
			denuncia.setLocalizacao(loc)
			denuncia.setPerfil(perfil)
			denuncia.setImagem(imagem)
			denuncia.setTotalToDeOlho(0)
			denuncia.setTotalToNaMidia(0)
			denuncia.setResolvida(false)
			denuncia.setBanida(false)
			
			denuncia.save flush:true
			
			println "Executou sem erros..."
		
		}
		catch(Exception e){
			
			println "Entrou na exceção..."
			
			throw new RuntimeException(e.getMessage())
			
		}
		
	}
	
}
