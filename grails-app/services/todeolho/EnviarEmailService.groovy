package todeolho

import grails.plugin.mail.MailService
import grails.transaction.Transactional

class EnviarEmailService {

	boolean transactional = false

	def MailService mailService

	def enviar(remetentes, assunto , mensagem) {

		mailService.sendMail {
			to remetentes.toArray()
			subject assunto
			body mensagem
		}

	}
}
