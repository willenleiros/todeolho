<%@ page import="todeolho.Localizacao" %>



<div class="fieldcontain ${hasErrors(bean: localizacaoInstance, field: 'denuncias', 'error')} ">
	<label for="denuncias">
		<g:message code="localizacao.denuncias.label" default="Denuncias" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${localizacaoInstance?.denuncias?}" var="d">
    <li><g:link controller="denuncia" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="denuncia" action="create" params="['localizacao.id': localizacaoInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'denuncia.label', default: 'Denuncia')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: localizacaoInstance, field: 'latitude', 'error')} required">
	<label for="latitude">
		<g:message code="localizacao.latitude.label" default="Latitude" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="latitude" value="${fieldValue(bean: localizacaoInstance, field: 'latitude')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: localizacaoInstance, field: 'longitude', 'error')} required">
	<label for="longitude">
		<g:message code="localizacao.longitude.label" default="Longitude" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="longitude" value="${fieldValue(bean: localizacaoInstance, field: 'longitude')}" required=""/>

</div>

