<%@ page import="todeolho.Localizacao"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="crud">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'localizacao.label', default: 'Localizacao')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div id="edit-localizacao" class="content scaffold-edit"
						role="main">
						<h1>
							<g:message code="default.edit.label" args="[entityName]" />
						</h1>
						<g:if test="${flash.message}">
							<div class="message" role="status">
								${flash.message}
							</div>
						</g:if>
						<g:hasErrors bean="${localizacaoInstance}">
							<ul class="errors" role="alert">
								<g:eachError bean="${localizacaoInstance}" var="error">
									<li
										<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
											error="${error}" /></li>
								</g:eachError>
							</ul>
						</g:hasErrors>
						<g:form url="[resource:localizacaoInstance, action:'update']"
							method="PUT">
							<g:hiddenField name="version"
								value="${localizacaoInstance?.version}" />
							<fieldset class="form">
								<g:render template="form" />
							</fieldset>
							<fieldset class="buttons">
								<g:actionSubmit class="save" action="update"
									value="${message(code: 'default.button.update.label', default: 'Update')}" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
