
<%@ page import="todeolho.Localizacao" %>
<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta name="layout" content="main">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<g:set var="entityName" value="${message(code: 'localizacao.label', default: 'Localizacao')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-localizacao" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-localizacao" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

						<g:sortableColumn property="latitude" title="${message(code: 'localizacao.latitude.label', default: 'Latitude')}" />

						<g:sortableColumn property="longitude" title="${message(code: 'localizacao.longitude.label', default: 'Longitude')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${localizacaoInstanceList}" status="i" var="localizacaoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td><g:link action="show" id="${localizacaoInstance.id}">${fieldValue(bean: localizacaoInstance, field: "latitude")}</g:link></td>

						<td>${fieldValue(bean: localizacaoInstance, field: "longitude")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${localizacaoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
