<%@ page import="todeolho.Denuncia"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templateDenuncia">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'denuncia.label', default: 'Denuncia')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
<script type="text/javascript"
	src="${resource(dir:'js/',file:'mapa.js')}"></script>
<script type="text/javascript"
	src="${resource(dir:'js/',file:'jquery-ui.custom.min.js')}"></script>
<style type="text/css">
#mapa {
	width: 100%;
	height: 250px;
	border: 10px solid #026060;
	margin-bottom: 20px;
}
.campos input[type="text"] {
	background: #FFF;
	color: #666;
	display: block;
	float: left;
	font: 15px 'Open Sans',Arial;
	height: 16px;
	padding: 9px;
	width: 475px;
}
.ui-autocomplete {
	background: #fff;
	border-top: 1px solid #ccc;
	cursor: pointer;
	font: 15px 'Open Sans',Arial;
	margin-left: 3px;
	width: 493px !important;
	position: fixed;
}

.ui-autocomplete .ui-menu-item {
	list-style: none outside none;
	padding: 7px 0 9px 10px;
}

.ui-autocomplete .ui-menu-item:hover {
	background: #eee;
}

.ui-autocomplete .ui-corner-all {
	color: #666 !important;
	display: block;
}
</style>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="section-title center">
						<h2>
							Edite <strong>sua denúncia</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
						<small><em>Fique a vontade para registrar suas denúncias e exibir os problemas sociais
									de nossa cidade. No entanto não concordamos com a adição imagens obscenas ou
									que venha denigrir a imagem de outras pessoas, vindo de encontro a <a href="#">política de
									privacidade do ToDeOlho</em></small>
					</div>


					<g:if test="${flash.message}">
						<div class="message" role="status">
							${flash.message}
						</div>
					</g:if>
					<g:hasErrors bean="${denunciaInstance}">
						<ul class="errors" role="alert">
							<g:eachError bean="${denunciaInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</g:hasErrors>
					<g:form url="[resource:denunciaInstance, action:'update']" enctype="multipart/form-data" accept-charset="UTF-8">
						<g:hiddenField name="version" value="${denunciaInstance?.id}" />
						<fieldset class="form">
							<g:render template="editForm" />
						</fieldset>
						<button type="submit" class="btn tf-btn btn-default">Atualizar</button>
					</g:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
