<%@ page import="todeolho.Denuncia"%>

<div class="form-group">
	<label for="txtEndereco">Digite a localização</label>
	<g:textField id="txtEndereco" name="txtEndereco" class="form-control" required=""
		value="${denunciaInstance?.localizacao?.txtEndereco}" />

</div>

<div id="mapa"></div>
<g:hiddenField id="id" name="id" required=""
	value="${denunciaInstance?.localizacao?.id}" />
<g:hiddenField id="txtLatitude" name="txtLatitude" required=""
	value="${denunciaInstance?.localizacao?.txtLatitude}" />
<g:hiddenField id="txtLongitude" name="txtLongitude" required=""
	value="${denunciaInstance?.localizacao?.txtLongitude}" />

<g:hiddenField id="perfil" name="perfil.id" required=""
	value="${denunciaInstance?.perfil?.id}" />

<div
	class="fieldcontain ${hasErrors(bean: denunciaInstance, field: 'categoria', 'error')} required">
	<div class="form-group">
		<label for="categoria"> <g:message
				code="denuncia.categoria.label" default="Categoria" /> <span
			class="required-indicator">*</span>
		</label>
		<g:select id="categoria" name="categoria.id"
			from="${todeolho.Categoria.list()}" optionKey="id" required=""
			value="${denunciaInstance?.categoria?.id}" class="many-to-one form-control" />
	</div>
</div>

<!--<div
	class="fieldcontain ${hasErrors(bean: denunciaInstance, field: 'imagem', 'error')} required">
	<div class="form-group">

		<label for="imagem"><g:message code="denuncia.imagem.label"
				default="Imagem" /></label> <input type="file" id="imagem" name="imagem" class="form-control" />

	</div>
</div>-->

<div
	class="fieldcontain ${hasErrors(bean: denunciaInstance, field: 'descricao', 'error')} required">
	<div class="form-group">
		<label for="descricao"> <g:message
				code="denuncia.descricao.label" default="Descrição" /> <span
			class="required-indicator">*</span>
		</label>
		<g:textField class="form-control" name="descricao" required=""
			value="${denunciaInstance?.descricao}" />
	</div>
</div>
