<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="todeolho.Denuncia"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templateDenuncia">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'denuncia.label', default: 'Denúncia')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
</head>
<body>
	<br/>
	<br/>
	<br/>
	<div id="tf-contact" class="text-center">
		<div class="container">

			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="section-title center">
						<h2>
							Detalhes <strong>da denúncia</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
					</div>

					<g:if test="${denunciaInstance.banida == false}">
						<div id="show-denuncia" role="main">
							<g:if test="${flash.message}">
								<div class="message erro" role="status">
									${flash.message}
								</div>
							</g:if>

							<g:if test="${denunciaInstance?.imagem}">
								<p class="fieldcontain">
									<img
										src="${createLink(controller:'denuncia', action:'imagem',id:denunciaInstance?.id)}"
										class="img-responsive" />
								</p>
							</g:if>
							<g:else>
								<p class="fieldcontain">
									<img src="${resource(dir:'images/logo/',file:'camera.jpg')}"
										class="img-responsive" style="width: 660px; height: 660px;" />
								</p>
							</g:else>

							<g:if test="${denunciaInstance?.categoria}">
								<p class="fieldcontain">
									<span id="categoria-label" class="property-label">Categoria:
									</span> <span class="property-value" aria-labelledby="categoria-label">
										${denunciaInstance?.categoria?.nome}
									</span>
								</p>
							</g:if>

							<g:if test="${denunciaInstance?.descricao}">
								<p class="fieldcontain">
									<span id="descricao-label" class="property-label">Descrição:
									</span> <span class="property-value" aria-labelledby="descricao-label"><g:fieldValue
											bean="${denunciaInstance}" field="descricao" /></span>
								</p>
							</g:if>

							<g:if test="${denunciaInstance?.localizacao}">
								<p class="fieldcontain">
									<span id="localizacao-label" class="property-label">
										Localização: </span> <span class="property-value"
										aria-labelledby="localizacao-label"> ${denunciaInstance?.localizacao?.txtEndereco}
									</span>
								</p>
							</g:if>

							<g:if test="${denunciaInstance?.resolvida}">
								<p class="fieldcontain">
									<span id="status-label" class="property-label"
										style="color: red;"> Essa denúncia foi finalizada </span> <span
										class="property-value" aria-labelledby="localizacao-label">

									</span>
								</p>

							</g:if>

							<div>
								<g:if test="${denunciaInstance?.resolvida == false}">
								
									<sec:ifAllGranted roles="ROLE_CIDADAO">
										<a
										href="${createLink(uri: '/ToDeOlho/curtidaToDeOlho/')}${denunciaInstance.id}"
										class="btn tf-btn btn-warming"
										data-toggle="tooltip" title="Tô de olho"> <i class="fa fa-eye fa-2x"></i>
										${denunciaInstance?.curtidaToDeOlho.size()}
										</a>
										
										<a
										href="${createLink(uri: '/ToNaMidia/curtidaToNaMidia/')}${denunciaInstance.id}"
										class="btn tf-btn btn-warming disabled"
										data-toggle="tooltip" title="Tô na mídia"> <i
										class="fa fa-desktop fa-2x"></i> ${denunciaInstance?.curtidaToNaMidia.size()}

										</a>		
   									</sec:ifAllGranted>
									
									<sec:ifAllGranted roles="ROLE_MIDIA">
										<a
										href="${createLink(uri: '/ToDeOlho/curtidaToDeOlho/')}${denunciaInstance.id}"
										class="btn tf-btn btn-warming disabled"
										data-toggle="tooltip" title="Tô de olho"> <i class="fa fa-eye fa-2x"></i>
										${denunciaInstance?.curtidaToDeOlho.size()}
										</a>
										
										<a
										href="${createLink(uri: '/ToNaMidia/curtidaToNaMidia/')}${denunciaInstance.id}"
										class="btn tf-btn btn-warming"
										data-toggle="tooltip" title="Tô na mídia"> <i
										class="fa fa-desktop fa-2x"></i> ${denunciaInstance?.curtidaToNaMidia.size()}

										</a>
   									</sec:ifAllGranted>
									

									<g:if test="${denunciaInstance?.perfil == perfilLoggedInUser}">
										<g:link action="edit" controller="denuncia" class="btn tf-btn btn-warming"
											data-toggle="tooltip" title="Editar"
											resource="${denunciaInstance}">
											<i class="fa fa-pencil-square-o fa-2x"></i>
										</g:link>
										<g:link controller="denuncia" action="apagar"
											class="btn tf-btn btn-warming"
											data-toggle="tooltip" title="Excluir"
											resource="${denunciaInstance}"
											onclick="return confirm('Confirme a exclusão do registro!');">
											<i class="fa fa-trash-o fa-2x"></i>
										</g:link>
									</g:if>

									<a
										href="${createLink(uri: '/comentario/create/')}${denunciaInstance.id}"
										class="btn tf-btn btn-warming"
										data-toggle="tooltip" title="Comentar"><i
										class="fa fa-comment-o fa-2x"></i> ${denunciaInstance?.comentarios.size()}
									</a>

<%--									<g:if test="${denunciaInstance?.perfil.id == session.user.id}">--%>
<%--										<g:link action="remove" class="btn tf-btn btn-warming"--%>
<%--											title="Compartilhar denúncia" resource="${denunciaInstance}">--%>
<%--											<i class="fa fa-share-alt fa-2x"></i>--%>
<%--										</g:link>--%>
<%--									</g:if>--%>
										<br/>
										<g:if test="${denunciaInstance.curtidaToDeOlho}">
											<a>Os cidadões </a>
											<g:each in="${denunciaInstance.curtidaToDeOlho}" var="curtida">
												<small><em>${curtida.perfil.nome}</em></small>
											</g:each>
											<a> estão de olho nessa denúncia.</a>
										</g:if>
										<br/>
										<g:if test="${denunciaInstance.curtidaToNaMidia}">
											<a>As mídias </a>
											<g:each in="${denunciaInstance.curtidaToNaMidia}" var="curtida">
												<small><em>${curtida.perfil.nome}</em></small>
											</g:each>
											<a> estão de olho nessa denúncia.</a>
										</g:if>

								</g:if>
							</div>

						</div>
					</g:if>
					<g:else>
						<div style="color: red;">Essa denúncia foi banida do sistema</div>
					</g:else>

				</div>
			</div>



			<g:if test="${denunciaInstance.banida == false}">

				<g:if test="${!denunciaInstance?.comentarios.isEmpty()}">
					<hr />

					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="section-title center">
								<h2>
									<strong>Comentários</strong>
								</h2>
								<div class="line">
									<hr>
								</div>
								<div class="clearfix"></div>
							</div>

							<g:each in="${denunciaInstance.comentarios}"
								var="comentarioInstance">

								<div class="media">
									<div class="media-left">
										<g:if test="${comentarioInstance?.perfil.imagem}">
											<img
												src="${createLink(controller:'perfil', action:'imagem',id:comentarioInstance?.perfil?.id)}"
												class="media-object" style="width: 64px; height: 64px;" />

										</g:if>
										<g:else>
											<img
												src="${resource(dir:'images/team/',file:'blank.jpg')}"
												class="media-object" style="width: 64px; height: 64px;" />
										</g:else>
									</div>
									<div class="media-body">
										<div class="media-left">
											<h4 class="media-left">
												${comentarioInstance?.perfil?.nome}
											</h4>

											<p>
												${comentarioInstance?.texto}
											</p>


											<g:if test="${comentarioInstance?.melhorComentario}">
												<p style="color: red;">Esse comentário foi avaliado como
													o melhor.</p>
											</g:if>


											<p class="media-left">
												<span style="font-style: italic; font-size: small;">
													${comentarioInstance?.lastUpdated}
												</span>
												<g:if test="${denunciaInstance?.resolvida == false}">

													<g:if
														test="${comentarioInstance?.perfil == perfilLoggedInUser}">
														<a
															href="${createLink(uri: '/comentario/edit/')}${comentarioInstance.id}"
															data-toggle="tooltip" title="Editar"
															class="btn tf-btn btn-warming"> <i
															class="fa fa-pencil-square"></i>
														</a>

														<g:link action="remove"
																class="btn tf-btn btn-warming"
																data-toggle="tooltip" title="Excluir"
																resource="${comentarioInstance}"
																onclick="return confirm('Confirme a exclusão do registro!');">
																<i class="fa fa-trash"></i>
														</g:link>

													</g:if>
													
													<sec:ifAllGranted roles="ROLE_CIDADAO">
														<g:if
															test="${denunciaInstance?.perfil == perfilLoggedInUser}">
															<g:link action="finalizarSatisfeito"
																class="btn tf-btn btn-warming"
																data-toggle="tooltip" title="Avaliar Satisfeito"
																resource="${comentarioInstance}">
																<i class="fa fa-check-square-o"></i>
															</g:link>
															<g:link action="finalizarInsatisfeito"
																class="btn tf-btn btn-warming"
																data-toggle="tooltip" title="Avaliar Insatisfeito"
																resource="${denunciaInstance}">
																<i class="fa fa-thumbs-down"></i>
															</g:link>

														</g:if>		
   													</sec:ifAllGranted>

												</g:if>
											</p>
										</div>
									</div>
								</div>
								<hr />
							</g:each>
						</div>
					</div>
					<!-- row -->
				</g:if>
			</g:if>
		</div>
		<!-- conteiner -->
	</div>
</body>
</html>
