<%@ page import="todeolho.Denuncia"%>
<%@ page import="todeolho.Categoria"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templateDenuncia">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<br />
	<br />
	<br />
	<div id="tf-works">
		<div class="container">
			<!-- Container -->
			<div class="section-title text-center center">
				<h2>
					Últimas <strong>Denúncias</strong>
				</h2>
				<div class="line">
					<hr>
				</div>
				<div class="clearfix"></div>
				<g:if test="${flash.message}">
					<div class="message" role="status">
						${flash.message}
					</div>
				</g:if>
			</div>

			<div class="categories">

				<ul class="cat">
					<li class="pull-left"><h4>Filtrar por categoria:</h4></li>
					<li class="pull-right">
						<ol class="type">
							<li><a href="#" data-filter="*" class="active">Todas</a></li>
							<%
			  					def categorias = Categoria.list()

								for (categoria in categorias){


			   				%>

			   					<li><a href="#" data-filter=".${categoria.nome}">${categoria.nome}</a></li>

			   				<%

								}
			    			%>
						</ol>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div id="lightbox" class="row">

				<g:each in="${denunciaInstanceList}" status="i"
					var="denunciaInstance">
					<div
						class="col-sm-6 col-md-3 col-lg-3 ${fieldValue(bean: denunciaInstance, field: "categoria")}">
						<div class="portfolio-item">
							<div class="hover-bg">
								<g:link action="show" id="${denunciaInstance.id}">
									<div class="hover-text">
										<h4>
											${fieldValue(bean: denunciaInstance, field: "categoria")}
										</h4>
										<div>
											<span class="li_eye" style="display: inline;">
												${denunciaInstance?.curtidaToDeOlho.size()}
											</span>
											<span class="li_tv">
												${denunciaInstance?.curtidaToNaMidia.size()}
											</span>
											<span class="li_bubble">
												${(denunciaInstance?.comentarios).size()}
											</span>
										</div>
										<small>Saiba mais</small>
										<div class="clearfix"></div>
										<i class="fa fa-plus"></i>
									</div>
									<g:if test="${denunciaInstance?.imagem}">
										<p class="fieldcontain">
											<img
												src="${createLink(controller:'denuncia', action:'imagem',id:denunciaInstance?.id)}"
												class="img-responsive" />
										</p>
									</g:if>
									<g:else>
										<p class="fieldcontain">
											<img src="${resource(dir:'images/logo/',file:'camera.jpg')}"
												class="img-responsive" />
										</p>
									</g:else>

								</g:link>
							</div>
						</div>
					</div>
				</g:each>	
			</div>
			<div id="paginacao" style="text-align:center;font-size:16px;">
					<g:paginate next="Proxima" prev="Anterior"
    							maxsteps="0" controller="denuncia"
            					action="index" total="${denunciaInstanceCount}" />
            </div>		
		</div>
	</div>
</body>
</html>
