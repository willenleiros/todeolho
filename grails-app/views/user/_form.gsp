<%@ page import="todeolho.User" %>
<%@ page import="todeolho.Role" %>



<div class="form-group">
	<label for="username">
		Nome do usuário
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="username" required="" value="${userInstance?.username}"/>

</div>

<div class="form-group">
	<label for="password">
		Senha
		<span class="required-indicator">*</span>
	</label>
	<g:passwordField class="form-control" name="password" required="" value="${userInstance?.password}"/>

</div>

<div class="form-group">
	<label>Tipo de Perfil*</label>
	<div class="controls">
		<select name="tipoPerfil" class="form-control" required>
			<g:each var="role" in="${Role.list()}">
				<g:if test="${!role.authority.equals("ROLE_ADMIN")}">
					<option value="${role.id}">${role.authority.substring(5)}</option>
				</g:if>
			</g:each>
		</select>
	</div>
</div>


<g:hiddenField name="accountExpired" value="${userInstance?.accountExpired}" />

<g:hiddenField name="accountLocked" value="${userInstance?.accountLocked}" />

<g:hiddenField name="enabled" value="${userInstance?.enabled}" />

<g:hiddenField name="passwordExpired" value="${userInstance?.passwordExpired}" />



