
<%@ page import="todeolho.Comentario" %>
<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta name="layout" content="templateComentario">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<g:set var="entityName" value="${message(code: 'comentario.label', default: 'Comentario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-comentario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list comentario">
			
				<g:if test="${comentarioInstance?.texto}">
				<li class="fieldcontain">
					<span id="texto-label" class="property-label"><g:message code="comentario.texto.label" default="Texto" /></span>
					
						<span class="property-value" aria-labelledby="texto-label"><g:fieldValue bean="${comentarioInstance}" field="texto"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${comentarioInstance?.denuncia}">
				<li class="fieldcontain">
					<span id="denuncia-label" class="property-label"><g:message code="comentario.denuncia.label" default="Denuncia" /></span>
					
						<span class="property-value" aria-labelledby="denuncia-label"><g:link controller="denuncia" action="show" id="${comentarioInstance?.denuncia?.id}">${comentarioInstance?.denuncia?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:comentarioInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${comentarioInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
