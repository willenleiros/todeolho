<%@ page import="todeolho.Comentario"%>



<div
	class="fieldcontain ${hasErrors(bean: comentarioInstance, field: 'texto', 'error')} required">
	<div class="form-group">
		<label for="texto"> <g:message code="comentario.texto.label"
				default="Texto" /> <span class="required-indicator">*</span>
		</label>
		<g:textArea class="form-control" name="texto" required=""
			value="${comentarioInstance?.texto}" style="height:250px;" />
	</div>
</div>

<div
	class="fieldcontain ${hasErrors(bean: comentarioInstance, field: 'denuncia', 'error')} required">
	<div class="form-group">
		<g:hiddenField id="denuncia" name="denuncia.id" required=""
			value="${comentarioInstance?.denuncia?.id}"
			class="many-to-one form-control" />
	</div>
</div>

