<%@ page import="todeolho.Comentario"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templateComentario">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'comentario.label', default: 'Comentario')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="section-title center">
						<h2>
							Edite <strong>sua denúncia</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
					</div>
					<div id="edit-comentario" role="main">
						<g:if test="${flash.message}">
							<div class="message" role="status">
								${flash.message}
							</div>
						</g:if>
						<g:hasErrors bean="${comentarioInstance}">
							<ul class="errors" role="alert">
								<g:eachError bean="${comentarioInstance}" var="error">
									<li
										<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
											error="${error}" /></li>
								</g:eachError>
							</ul>
						</g:hasErrors>
						<g:form url="[resource:comentarioInstance, action:'update']"
							method="PUT" accept-charset="UTF-8">
							<g:hiddenField name="version"
								value="${comentarioInstance?.version}" />
							<fieldset class="form">
								<g:render template="form" />
							</fieldset>
							<fieldset class="buttons">
								<button type="submit" class="btn tf-btn btn-default">Atualizar</button>
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
