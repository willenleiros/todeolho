<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="Sistema inteligente de gerenciamento de denúncias urbanas com geolocalização.">
		<meta name="author" content="[IFRN 2015.1 PDS-Distribuido] Cristiane, Icaro, Willen">
		<link rel="icon" href="/favicon.ico">

		<title>Tô de Olho | API</title>

		<!-- CSS -->
		<link href="/web/css/bootstrap.min.css" rel="stylesheet">
		<link href="/web/css/font-awesome.min.css" rel="stylesheet">
		<link href="/web/css/theme.css" rel="stylesheet">

		<!-- Fonts -->
		<!--
			<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600' rel='stylesheet' type='text/css'>
		-->
		<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,100italic,300italic,400italic,700italic' rel='stylesheet' type='text/css'>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<!-- Fixed navbar -->
		<nav class="navbar navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<!-- Collapse button (smartphone-view) -->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/web/"><i class="fa fa-eye"></i> Tô de Olho</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						<li><a href="/web/">Denúncias</a></li>
						<li><a href="/web/#tf-team">Equipe</a></li>
						<li><a href="/web/#tf-contact">Contato</a></li>
						<li><a href="/web/blog">Blog</a></li>
						<li class="active"><a href="/api">API</a></li>
						<li><a href="/web/usuario/login" class="login-button">Login</a></li>
						
						<!--
						<li class="profile dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								<span><img src="http://www.gravatar.com/avatar/76ab1328bb0c6feb4120c25c4abde073?s=32" class="img-circle" /> 
								Ícaro R. Scherma</span>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu pull-right">
								<li><a href="/denuncias"><i class="fa fa-eye"></i> Minhas denúncias</a></li>
								<li><a href="/perfil"><i class="fa fa-user"></i> Perfil</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="/logout"><i class="fa fa-bug"></i> Denunciar bug</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
							</ul>
						</li>

						-->
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>

		<!-- Begin page content -->
		<div class="container">
			<div class="page-header">
				<h2>Documentação da <strong>API</strong></h2>
			</div>
			<div class="row">
				<div class="col-md-3">
					<h3>Índices</h3>
					<ul id="api-affix">
						<li><a href="#overview">Visão Geral</a></li>
						<li><a href="#updates">Atualizações</a></li>
						<li><a href="#authentication">Autenticação</a></li>
						<li><a href="#rate-limiter">Limites de Acesso</a></li>
						<li><a href="#policy">Políticas</a></li>
						<li><a href="#feedback">Feedback</a></li>
						<li><a href="#routes">Mapeamento de Recursos</a></li>
					</ul>
				</div>
				<div class="col-md-9">
					<h3 id="overview">Visão Geral</h3>
					<p>
						Esta <abbr title="Application Program Interface">API</abbr> <abbr title="Representational State Transfer">REST</abbr> visa prover recursos o suficientes para que aplicações de terceiros possam interagir com nosso sistema e de uma forma inteligente compartilhemos recursos para ajudar a tornar nosso mundo melhor.
					</p>

					<h3 id="updates">Atualizações</h3>
					<p>
						<ul>
							<li>1.0 - Versão inicial</li>
							<p>
								Versão inicial não-RESTful suportando JSON.
							</p>
						</ul>
					</p>

					<h3 id="authentication">Autenticação</h3>
					<p>
						A autenticação é feita através do padrão <abbr title="JSON Web Token">JWT</abbr>, e é necessário um registro prévio no sistema para então obter o acesso a API. Após ter se registrado, no menu de perfil você deverá ver sua chave de acesso.
					</p>

					<h3 id="rate-limiter">Limites de Acesso</h3>
					<p>
						Nesta versão <strong>não estamos limitando</strong> a quantidade de acesso ao sistema, porém caso notemos que esteja usando de forma mal-intencionada ou um grande número de acesso esteja tornando o sistema mais lento, iremos tentar entrar em contato com o usuário.
					</p>

					<h3 id="policy">Políticas</h3>
					<p>
						Nossa política é a <strong>Don't be Evil</strong>, onde deverá usar de forma sensata e assim que detectar algum bug ou falha no sistema (principalmente de segurança) pedimos que entre em contato conosco.
					</p>

					<h3 id="feedback">Feedback</h3>
					<p>
						Nós disponibilizamos a API de uma forma <em>AS IS</em>, ou seja, não garantimos funcionalidade 24/7 ou alguma duração de cada versão, mas avisaremos com antecedência aos usuários caso precisemos atualizar e alguma rota ou função acabe sendo abandonada.
					</p>

					<h3 id="routes">Mapeamento de Recursos</h3>
					<p>
						<ul>
							<li>Categorias</li>
							<ul>
								<li>GET <code>categoria/todas</code></li>
							</ul>

							<li>Denúncias</li>
							<ul>
								<li>GET <code>denuncia/&amp;id</code></li>
								<li>GET <code>denuncia/listarRecentes</code></li>
								<li>GET <code>denuncia/listarResolvidas</code></li>
								<li>GET <code>denuncia/listarNaoResolvidas</code></li>
								<li>GET <code>denuncia/listarPorCategoria/&amp;descricaoDaCategoria</code></li>
								<li>GET <code>denuncia/listarPorBairro/&amp;descricaoDoBairro</code></li>
							</ul>

						</ul>
					</p>


				</div>
			</div>
		</div>

		<!-- Footer -->
		<footer class="footer">
			<div class="container">
				<p class="pull-left">
					<strong>Copyright</strong> <em>Tô de Olho</em> &copy; 2015 - Todos os direitos reservados.
				</p>
				<ul class="social-media pull-right">
					<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
				</ul>
			</div>
		</footer>

		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="/web/js/bootstrap.min.js"></script>
		<script src="/web/js/theme.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="/web/s/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>

