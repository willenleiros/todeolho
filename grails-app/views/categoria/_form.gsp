<%@ page import="todeolho.Categoria"%>

<div
	class="fieldcontain ${hasErrors(bean: categoriaInstance, field: 'nome', 'error')} required">
	<div class="form-group">
		<label for="nome"> <g:message code="categoria.nome.label"
				default="Nome" /> <span class="required-indicator">*</span>
		</label>
		<g:textField name="nome" required="" class="form-control"
			value="${categoriaInstance?.nome}" />
	</div>
</div>

<div
	class="fieldcontain ${hasErrors(bean: categoriaInstance, field: 'descricao', 'error')} required">
	<div class="form-group">
		<label for="descricao"> <g:message
				code="categoria.descricao.label" default="Descricao" /> <span
			class="required-indicator">*</span>
		</label>
		<g:textField name="descricao" required="" class="form-control"
			value="${categoriaInstance?.descricao}" />
	</div>
</div>



