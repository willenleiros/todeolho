
<%@ page import="todeolho.Categoria"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templateCategoria">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'categoria.label', default: 'Categoria')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="section-title center">
						<h2>
							Detalhes <strong>da categoria</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
					</div>

					<div id="show-categoria" role="main">
						<g:if test="${flash.message}">
							<div class="message" role="status">
								${flash.message}
							</div>
						</g:if>

						<g:if test="${categoriaInstance?.descricao}">
							<p class="fieldcontain"><span id="descricao-label"
								class="property-label">Descrição: </span> <span
								class="property-value" aria-labelledby="descricao-label"><g:fieldValue
										bean="${categoriaInstance}" field="descricao" /></span></p>
						</g:if>

						<g:if test="${categoriaInstance?.nome}">
							<p class="fieldcontain">
								<span id="nome-label" class="property-label">Nome: </span> <span
									class="property-value" aria-labelledby="nome-label"><g:fieldValue
										bean="${categoriaInstance}" field="nome" /></span>
							</p>
						</g:if>


						<g:form url="[resource:categoriaInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="edit" action="edit" class="btn tf-btn btn-default"
									resource="${categoriaInstance}">
									Editar
								</g:link>
								<g:actionSubmit class="btn tf-btn btn-danger" action="delete"
									value="Excluir"
									onclick="return confirm('Confirme a exclusão do registro!');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
