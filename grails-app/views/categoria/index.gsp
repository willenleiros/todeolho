<%@ page import="todeolho.Categoria"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templateCategoria">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'categoria.label', default: 'Categoria')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="section-title center">
						<h2>
							Detalhes <strong>da categoria</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
					</div>

					<div id="list-categoria" role="main">
						<g:if test="${flash.message}">
							<div class="message" role="status">
								${flash.message}
							</div>
						</g:if>
						<table class="table table-bordered">
							<thead>
								<tr>

									<g:sortableColumn property="descricao"
										title="${message(code: 'categoria.descricao.label', default: 'Descricao')}" />

									<g:sortableColumn property="nome"
										title="${message(code: 'categoria.nome.label', default: 'Nome')}" />

								</tr>
							</thead>
							<tbody>
								<g:each in="${categoriaInstanceList}" status="i"
									var="categoriaInstance">
									<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

										<td><g:link action="show" id="${categoriaInstance.id}">
												${fieldValue(bean: categoriaInstance, field: "descricao")}
											</g:link></td>

										<td>
											${fieldValue(bean: categoriaInstance, field: "nome")}
										</td>

									</tr>
								</g:each>
							</tbody>
						</table>
						<div class="pagination">
							<g:paginate total="${categoriaInstanceCount ?: 0}" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
