<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="main">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<br />
	<br />
	<br />
	<div id="tf-works">
		
		
     		<div class="container">
        		<h1>Atenção!</h1>
        		<p>
        			Desculpe-nos, mas você não possui autorização para acessar essa página.
        		</p>
        		<p>
        			Caso você acredite que possui esse acesso entre em contato com a equipe de desenvolvimento para que possamos analisar seu nível de acesso.
        		</p>
        		<form enctype="text/plain" method="get" action="mailto:contato@todeolho.net.br">
					<fieldset class="form">
						<button type="submit" class="btn tf-btn btn-default">contato@todeolho.net.br</button>
					</fieldset>
				</form>
      		</div>
    	
		
	</div>
</body>
</html>
