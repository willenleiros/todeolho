<%@ page import="todeolho.Denuncia"%>
<%@ page import="todeolho.Categoria"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="main">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tô de Olho</title>
<style>
html, body, #map {
	height: 400px;
	margin: 0;
	padding: 0;
}
</style>
</head>

<body>
	<div id="map"></div>
	<script>

	//Contantes
	const PROTOCOLO = "http";
	const NOMEPROJETO = "web";
	const HOST = window.location.host;

	var map, heatmap;

	function initMap() {
  		map = new google.maps.Map(document.getElementById('map'), {
    		zoom: 11,
    		center: {lat: -5.791533, lng: -35.204156},
    		mapTypeId: google.maps.MapTypeId.TERRAIN
  		});

  		buscarPontos();

	}

	function irParaVejaMais(){
		const API = "denuncia/index";
		
		var url = PROTOCOLO + "://" + HOST + "/" + NOMEPROJETO + "/" + API;

		window.location=url;
	}

	function changeGradient() {
		  var gradient = [
		    'rgba(0, 255, 255, 0)',
		    'rgba(0, 255, 255, 1)',
		    'rgba(0, 191, 255, 1)',
		    'rgba(0, 127, 255, 1)',
		    'rgba(0, 63, 255, 1)',
		    'rgba(0, 0, 255, 1)',
		    'rgba(0, 0, 223, 1)',
		    'rgba(0, 0, 191, 1)',
		    'rgba(0, 0, 159, 1)',
		    'rgba(0, 0, 127, 1)',
		    'rgba(63, 0, 91, 1)',
		    'rgba(127, 0, 63, 1)',
		    'rgba(191, 0, 31, 1)',
		    'rgba(255, 0, 0, 1)'
		  ]
		  heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
		}

	function changeRadius() {
		heatmap.set('radius', heatmap.get('radius') ? null : 20);
	}

	function buscarPontos() {

		var itens = [];

		const API = "/api/anonymous/denuncia/mapaDeCalor";
		
		var url = PROTOCOLO + "://" + HOST + "/" + NOMEPROJETO + API;

		$.ajax({
			url : url,
			method : "GET",
			context : document.body
		}).done(function(resp) {
			console.log(resp);
			$.each( resp, function( key, val ) {

				itens.push(new google.maps.LatLng(val.txtLatitude, val.txtLongitude));
				
			});

			plotar(itens)
		
		}).fail(function() {

			console.log("error");

		});

	}

	function plotar(pontos){
		heatmap = new google.maps.visualization.HeatmapLayer({
    		data: pontos,
    		map: map
  		});

  		changeGradient();
  		changeRadius();
	}
    </script>
    <script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpOskFId50Rm56o3BueZmVoDEHclq2L2U&signed_in=true&libraries=visualization&callback=initMap">
    </script>

	<!-- Portfolio Section
    ==========================================-->
	<div id="tf-works">
		<div class="container">
			<!-- Container -->
			<div class="section-title text-center center">
				<h2>
					Denúncias <strong>Recentes</strong>
				</h2>
				<div class="line">
					<hr>
				</div>
				<div class="clearfix"></div>

			</div>

			<div class="categories">

				<ul class="cat">
					<li class="pull-left"><h4>Filtrar por categoria:</h4></li>
					<li class="pull-right">
						<ol class="type">
							<li><a href="#" data-filter="*" class="active">Todas</a></li>

							<%
			  		def categorias = Categoria.list()

					for (categoria in categorias){


			   %>

			   	<li><a href="#" data-filter=".${categoria.nome}">${categoria.nome}</a></li>

			   <%

					}
			    %>


						</ol>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div id="lightbox" class="row">

				<%
        def denuncias = Denuncia.withCriteria {

			maxResults(8)
			order("dateCreated", "desc")
		}

        for (denuncia in denuncias){
      %>
		<div class="col-sm-6 col-md-3 col-lg-3 ${denuncia.categoria.nome}">
			<g:link controller="denuncia" action="show" id="${denuncia.id}">
				<div class="portfolio-item">
				  <div class="hover-bg">
						
						<div class="hover-text">
							<h4>
							  ${denuncia.categoria.nome}
							</h4>
							<div>
							  <span class="li_eye" style="display: inline;">
							${denuncia?.totalToDeOlho}
							  </span> <span class="li_tv">
							${denuncia?.totalToNaMidia}
							  </span> <span class="li_bubble">
							${(denuncia?.comentarios).size()}
							  </span>
							</div>
							<small>Saiba mais</small>
							<div class="clearfix"></div>
							<i class="fa fa-plus"></i>
						  </div>
						  <g:if test="${denuncia?.imagem}">
							  
								  <img
								  src="${createLink(controller:'denuncia', action:'imagem',id:denuncia?.id)}"
								  alt="..." class="img-responsive" />
							 
						  </g:if>
						  <g:else>
							  <img src="${resource(dir:'images/logo/',file:'camera.jpg')}"
							  class="img-responsive" />
						  </g:else>
					  
					</div>
				</div>
			</g:link>
		  </div>
      <%
        }
      %>
			</div>
			<button id="vejaMais" type="submit" class="btn tf-btn btn-default" onclick="irParaVejaMais()">Veja mais</button>
		</div>
		<!-- conteiner -->
	</div>

	<!-- Team Page
    ==========================================-->
	<div id="tf-team" class="text-center">
		<div class="overlay">
			<div class="container">
				<div class="section-title center">
					<h2>
						Conheça <strong>nossa equipe</strong>
					</h2>
					<div class="line">
						<hr>
					</div>
				</div>

				<div id="team" class="owl-carousel owl-theme row">
					<div class="item">
						<div class="thumbnail">
							<img src="${resource(dir:'images/team/',file:'cris.jpg')}"
								alt="..." class="img-circle team-img">
							<div class="caption">
								<h3>Cristiane Viana</h3>
								<p>CEO / Fundadora</p>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="thumbnail">
							<img src="${resource(dir:'images/team/',file:'willen.jpg')}"
								alt="..." class="img-circle team-img">
							<div class="caption">
								<h3>Willen Leiros</h3>
								<p>CEO / Fundador</p>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="thumbnail">
							<img src="${resource(dir:'images/team/',file:'nayara.jpg')}"
								alt="..." class="img-circle team-img">
							<div class="caption">
								<h3>Nayara Rocha</h3>
								<p>CEO / Fundadora</p>
							</div>
						</div>
					</div>
					
					<div class="item">
						<div class="thumbnail">
							<img src="${resource(dir:'images/team/',file:'nirvana.jpg')}"
								alt="..." class="img-circle team-img">
							<div class="caption">
								<h3>Nirvana Dantas</h3>
								<p>CEO / Fundadora</p>
							</div>
						</div>
					</div>
					
					<div class="item">
						<div class="thumbnail">
							<img src="${resource(dir:'images/team/',file:'paulo.jpeg')}"
								alt="..." class="img-circle team-img">
							<div class="caption">
								<h3>Paulo Roberto</h3>
								<p>CEO / Fundador</p>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="thumbnail">
							<img src="${resource(dir:'images/team/',file:'blank.jpg')}"
								alt="..." class="img-circle team-img">
							<div class="caption">
								<h3>A comunidade</h3>
								<p>Delator</p>
								<p>Contribua você também.</p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Services Section
    ==========================================-->
	<div id="tf-services" class="text-center">
		<div class="container">
			<div class="section-title center">
				<h2>
					Dê uma <strong>olhada</strong> nas nossas <strong>aplicações</strong>
				</h2>
				<div class="line">
					<hr>
				</div>
				<div class="clearfix"></div>
				<small><em> Desenvolvemos (e ainda estamos
						desenvolvendo algumas) aplicações para ajudar a nos manter
						informados sobre problemas urbanos, através do uso correto de
						nossas aplicações poderemos contribuir com mídias locais e
						agilizar a solução de problemas urbanos. </em></small>
			</div>
			<div class="space"></div>
			<div class="row">
				<div class="col-md-3 col-sm-6 service">
					<i class="fa fa-desktop"></i>
					<h4>
						<strong>Web</strong>
					</h4>
					<p>A que está visualizando no momento, possuindo um ambiente
						gráfico para mensurar, pesquisar e ficar por dentro das
						denúncias da sua região.</p>
				</div>

				<div class="col-md-3 col-sm-6 service">
					<i class="fa fa-mobile"></i>
					<h4>
						<strong>Mobile</strong>
					</h4>
					<p>Quer ficar ainda mais antenado? Baixe também o app e receba
						notificações quando tiver novidades nas denúncias que segue.</p>
				</div>

				<div class="col-md-3 col-sm-6 service">
					<i class="fa fa-camera"></i>
					<h4>
						<strong>Tire fotos dos ambientes</strong>
					</h4>
					<p>As fotos nos ajudam a ter uma idéia de como está a denúncia
						atualmente.</p>
				</div>

				<div class="col-md-3 col-sm-6 service">
					<i class="fa fa-bullhorn"></i>
					<h4>
						<strong>Denuncie</strong>
					</h4>
					<p>E claro, não esqueça de denunciar todos os problemas urbanos
						da sua região, contamos com sua ajuda para melhorar a região.</p>
				</div>
			</div>
		</div>
	</div>

	<!-- Clients Section
    ==========================================-->
	<div id="tf-clients" class="text-center">
		<div class="overlay">
			<div class="container">

				<div class="section-title center">
					<h2>
						Desenvolvedor <strong>conheça nossa API</strong>
					</h2>
					<div class="line">
						<hr>
					</div>
				</div>
				<div id="clients" class="owl-carousel owl-theme">
					<div class="item">
						<h3>Gráficos</h3>
					</div>
					<div class="item">
						<h3>Mapa de calor</h3>
					</div>
					<div class="item">
						<h3>Estatísticas</h3>
					</div>
					<div class="item">
						<h3>Filtros por bairro</h3>
					</div>
					<div class="item">
						<h3>Em breve mais novidades</h3>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Contact Section
    ==========================================-->
	<div id="tf-contact" class="text-center">
		<div class="container">

			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="section-title center">
						<h2>
							Sinta-se livre para <strong>entrar em contato</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
						<small><em> Se tiver alguma dúvida, sugestão ou
								comentário, não hesite em entrar em contato, afinal, contamos
								contigo! ;) <br /> Não se preocupe, não compartilhamos seu
								e-mail com ninguém fora de nossa empresa, também odiamos SPAM.
								;)
						</em></small>
					</div>

					<form>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Nome completo</label> <input
										type="email" class="form-control" id="exampleInputEmail1"
										placeholder="Digite seu nome">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Endereço de e-mail</label> <input
										type="email" class="form-control" id="exampleInputEmail1"
										placeholder="Digite seu e-mail">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Mensagem</label>
							<textarea class="form-control" rows="3"></textarea>
						</div>

						<button type="submit" class="btn tf-btn btn-default">Enviar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
