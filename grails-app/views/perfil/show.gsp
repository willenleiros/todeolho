<%@ page import="todeolho.Perfil"%>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templatePerfil">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'perfil.label', default: 'Perfil')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="section-title center">
						<h2>
							Detalhes <strong>do Perfil</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
					</div>

					<div id="show-perfil" role="main">
						<g:if test="${flash.message}">
							<div class="message" role="status">
								${flash.message}
							</div>
						</g:if>
						
						<g:if test="${perfilInstance?.imagem}">
							<p class="fieldcontain">
								<span class="property-value" aria-labelledby="imagem-label">
									<img
									src="${createLink(controller:'perfil', action:'imagem',id:perfilInstance?.id)}"
									style="width: 60px; height: 60px;" />
								</span>
							</p>
						</g:if>

						<g:if test="${perfilInstance?.nome}">
							<p class="fieldcontain">
								<span id="nome-label" class="property-label">Nome: </span> <span
									class="property-value" aria-labelledby="nome-label"><g:fieldValue
										bean="${perfilInstance}" field="nome" /></span>
							</p>
						</g:if>

						<g:if test="${perfilInstance?.usuario}">
							<p class="fieldcontain">
								<span id="usuario-label" class="property-label">Login: </span> <span
									class="property-value" aria-labelledby="usuario-label">
									${perfilInstance?.usuario?.login}
								</span>
							</p>
						</g:if>

						<g:if test="${perfilInstance?.email}">
							<p class="fieldcontain">
								<span id="email-label" class="property-label">E-mail: </span> <span
									class="property-value" aria-labelledby="email-label"><g:fieldValue
										bean="${perfilInstance}" field="email" /></span>
							</p>
						</g:if>

						<g:if test="${perfilInstance?.dataNascimento}">
							<p class="fieldcontain">
								<span id="dataNascimento-label" class="property-label">Data
									de Nascimento: </span> <span class="property-value"
									aria-labelledby="dataNascimento-label"><g:formatDate
										date="${perfilInstance?.dataNascimento}" /></span>
							</p>
						</g:if>

						<g:if test="${perfilInstance?.cpf_cnpj}">
							<p class="fieldcontain">
								<span id="cpf_cnpj-label" class="property-label">CPF/CNPJ:
								</span> <span class="property-value" aria-labelledby="cpf_cnpj-label">
									<g:fieldValue bean="${perfilInstance}" field="cpf_cnpj" />
								</span>
							</p>
						</g:if>

						<g:if test="${perfilInstance?.status}">
							<p class="fieldcontain">
								<span id="status-label" class="property-label">Status: </span> <span
									class="property-value" aria-labelledby="status-label"><g:formatBoolean
										boolean="${perfilInstance?.status}" /></span>
							</p>
						</g:if>

						<g:if test="${perfilInstance?.tipoPerfil}">
							<p class="fieldcontain">
								<span id="tipoPerfil-label" class="property-label">Tipo
									Perfil: </span> <span class="property-value"
									aria-labelledby="tipoPerfil-label"><g:fieldValue
										bean="${perfilInstance}" field="tipoPerfil" /></span>
							</p>
						</g:if>

						<g:form url="[resource:perfilInstance, action:'delete']"
							method="DELETE">
							<fieldset class="buttons">
								<g:link class="edit" action="edit"
									class="btn tf-btn btn-default" resource="${perfilInstance}">
									Editar
								</g:link>
								<g:actionSubmit class="btn tf-btn btn-danger" action="delete"
									value="Excluir"
									onclick="return confirm('Confirme a exclusão do registro!');" />
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>