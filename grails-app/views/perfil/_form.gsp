
<%@ page import="todeolho.Perfil"%>


<div
	class="fieldcontain ${hasErrors(bean: perfilInstance, field: 'nome', 'error')} required">
	<div class="form-group">
		<label for="nome"> <g:message code="perfil.nome.label"
				default="Nome" /> <span class="required-indicator">*</span></label>
		<g:textField name="nome" required="" class="form-control"
			value="${perfilInstance?.nome}" />
	</div>
</div>

<div
	class="fieldcontain ${hasErrors(bean: perfilInstance, field: 'email', 'error')} required">
	<div class="form-group">
		<label for="email"> <g:message code="perfil.email.label"
				default="E-mail" /> <span class="required-indicator">*</span></label>
		<g:textField name="email" required="" class="form-control"
			value="${perfilInstance?.email}" />
	</div>
</div>

<div
	class="fieldcontain ${hasErrors(bean: perfilInstance, field: 'dataNascimento', 'error')} required">
	<div class="form-group">
		<label for="dataNascimento"> <g:message
				code="perfil.dataNascimento.label" default="Data de Nascimento" /></label>
		<g:datePicker name="dataNascimento" precision="day"
			value="${perfilInstance?.dataNascimento}" />
	</div>
</div>

<div
	class="fieldcontain ${hasErrors(bean: perfilInstance, field: 'cpf_cnpj', 'error')} required">
	<div class="form-group">
		<label for="cpf_cnpj"> <g:message code="perfil.cpf_cnpj.label"
				default="CPF/CNPJ" /> <span class="required-indicator">*</span>
		</label>
		<g:textField name="cpf_cnpj" required="" class="form-control"
			value="${perfilInstance?.cpf_cnpj}" />
	</div>
</div>

<div
	class="fieldcontain ${hasErrors(bean: perfilInstance, field: 'imagem', 'error')} required">
	<div class="form-group">
		<label for="imagem"><g:message code="perfil.imagem.label"
				default="Imagem" /></label> <input type="file" id="imagem" name="imagem"
			class="form-control" />
	</div>
</div>


