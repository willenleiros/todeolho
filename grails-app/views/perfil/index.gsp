<%@ page import="todeolho.Perfil"%>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templatePerfil">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'perfil.label', default: 'Perfil')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="section-title center">
						<h2>
							<strong>Perfil</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
					</div>


					<div id="list-perfil" class="content scaffold-list" role="main">
						<h1>
							<g:message code="default.list.label" args="[entityName]" />
						</h1>
						<g:if test="${flash.message}">
							<div class="message" role="status">
								${flash.message}
							</div>
						</g:if>
						<table class="table table-bordered">
							<thead>
								<tr>

									<g:sortableColumn property="nome"
										title="${message(code: 'perfil.nome.label', default: 'Nome')}" />

									<g:sortableColumn property="email"
										title="${message(code: 'perfil.email.label', default: 'Email')}" />

									<g:sortableColumn property="usuario"
										title="${message(code: 'perfil.usuario.label', default: 'Login')}" />
								</tr>
							</thead>
							<tbody>
								<g:each in="${perfilInstanceList}" status="i"
									var="perfilInstance">
									<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

										<td><g:link action="show" id="${perfilInstance.id}">
												${fieldValue(bean: perfilInstance, field: "nome")}
											</g:link></td>

										<td>
											${fieldValue(bean: perfilInstance, field: "email")}
										</td>

										<td>
											${fieldValue(bean: perfilInstance.usuario, field: "login")}
										</td>

									</tr>
								</g:each>
							</tbody>
						</table>
						<div class="pagination">
							<g:paginate total="${perfilInstanceCount ?: 0}" />
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</body>
</html>
