<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templatePerfil">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'perfil.label', default: 'Perfil')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">

			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="section-title center">
						<h2>
							Faça <strong>seu Cadastro</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
						<small><em>Lorem Ipsum comes from sections 1.10.32
								and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of
								Good and Evil) by Cicero, written in 45 BC. This book is a
								treatise on the theory of ethics, very popular during the
								Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor
								sit amet..", comes from a line in section 1.10.32.</em></small>
					</div>

					<g:if test="${flash.message}">
						<div class="message" role="status">
							${flash.message}
						</div>
					</g:if>
					<g:hasErrors bean="${perfilInstance}">
						<ul class="errors" role="alert">
							<g:eachError bean="${perfilInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</g:hasErrors>

					<g:form url="[resource:perfilInstance, action:'save']" enctype="multipart/form-data" accept-charset="UTF-8">
						<fieldset class="form">

							<g:render template="form" />
							<button type="submit" class="btn tf-btn btn-default">Salvar</button>
						</fieldset>
					</g:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
