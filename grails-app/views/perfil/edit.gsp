<%@ page import="todeolho.Perfil"%>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta name="layout" content="templatePerfil">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<g:set var="entityName"
	value="${message(code: 'perfil.label', default: 'Perfil')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
	<div id="tf-contact" class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="section-title center">
						<h2>
							Atualize <strong>seu Perfil</strong>
						</h2>
						<div class="line">
							<hr>
						</div>
						<div class="clearfix"></div>
						<small><em>Preencha os dados abaixo corretamente. 
						Você tem completa responsabilidade sobre os mesmos</em></small>
					</div>

					<div id="edit-perfil" role="main">
						<g:if test="${flash.message}">
							<div class="message" role="status">
								${flash.message}
							</div>
						</g:if>
						<g:hasErrors bean="${perfilInstance}">
							<ul class="errors" role="alert">
								<g:eachError bean="${perfilInstance}" var="error">
									<li
										<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
											error="${error}" /></li>
								</g:eachError>
							</ul>
						</g:hasErrors>
						<g:form url="[resource:perfilInstance, action:'update']" enctype="multipart/form-data" accept-charset="UTF-8">
							<g:hiddenField name="version"
								value="${perfilInstance?.version}" />
							<fieldset class="form">
								<g:render template="form" />
							</fieldset>
							<fieldset class="buttons">
								<button type="submit" class="btn tf-btn btn-default">Atualizar</button>
							</fieldset>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
