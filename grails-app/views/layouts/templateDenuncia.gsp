<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="pt-BR" class="no-js">
<!--<![endif]-->
<head>
<g:javascript library="jquery" plugin="jquery" />

<title>ToDeOlho</title>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="${resource(dir: 'css/', file: 'bootstrap.css')}" type="text/css">

<link rel="stylesheet"
	href="${resource(dir: 'fonts/font-awesome/css/', file: 'font-awesome.css')}"
	type="text/css">

<link rel="stylesheet"
	href="${resource(dir: 'lineicons/', file: 'style.css')}"
	type="text/css">

<!-- Slider
    ================================================== -->
<link rel="stylesheet"
	href="${resource(dir: 'css/', file: 'owl.carousel.css')}"
	type="text/css">

<link rel="stylesheet"
	href="${resource(dir: 'css/', file: 'owl.theme.css')}" type="text/css">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet"
	href="${resource(dir: 'css/', file: 'style.css')}" type="text/css">

<link rel="stylesheet"
	href="${resource(dir: 'css/', file: 'responsive.css')}" type="text/css">

<link
	href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400'
	rel='stylesheet' type='text/css'>

<style type="text/css">
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}

#tf-menu.navbar-default {
	background-color: #026060 !important;
	border-color: rgba(231, 231, 231, 0);
}
</style>

<script type="text/javascript"
	src="${resource(dir:'js/',file:'modernizr.custom.js')}"></script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrffjjhbO3f5nXcgrIsWfVOkabPLE8lBo"></script>

<g:layoutHead />
</head>
<body>

	<!-- Navigation
    ==========================================-->
	<nav id="tf-menu" class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${createLink(uri: '/')}">Tô De
					Olho</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					
					<sec:ifAnyGranted roles="ROLE_CIDADAO,ROLE_MIDIA,ROLE_PREFEITURA">
						<li><g:link class="page-scroll" controller="denuncia" action="index">Últimas Denúncias</g:link></li>	
   					</sec:ifAnyGranted>
								
					<sec:ifAllGranted roles="ROLE_CIDADAO">
						<li><g:link class="page-scroll" controller="denuncia" action="pessoais">Suas Denúncias</g:link></li>
						<li><g:link class="page-scroll" controller="denuncia" action="create">Nova Denúncia</g:link></li>		
   					</sec:ifAllGranted>
					
					<sec:ifLoggedIn>
						<li><a href="javascript:;"
							class="user-profile dropdown-toggle" data-toggle="dropdown"
							aria-expanded="false"> 
							
								<img src="${createLink(controller:'perfil', action:'imagemUserLogado')}" alt="Imagem Perfil">

								</a>
								<ul
								class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
								<li><a href="javascript:;">Olá Sr(a) <sec:loggedInUserInfo field="username"/></a></li>
								<li>
									<g:link controller="perfil" action="edit"> 
										<i class="fa fa-user pull-right"></i>Perfil</g:link>
								</li>
								<li>
									<g:link controller="logout">
										<i class="fa fa-sign-out pull-right"></i>Sair</g:link>
								</li>
							</ul>
						</li>
					</sec:ifLoggedIn>
					
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<g:layoutBody />

	<nav id="footer">
		<div class="container">
			<div class="pull-left fnav">
				<p>
					COPYRIGHT © 2016. <a
						href="https://dribbble.com/shots/1817781--FREEBIE-Spirit8-Digital-agency-one-page-template">Tô
						de Olho</a>

				</p>
			</div>
			<div class="pull-right fnav">
				<ul class="footer-social">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div>
		</div>
	</nav>

	<script type="text/javascript"
		src="${resource(dir:'js',file:'bootstrap.js')}"></script>

	<script type="text/javascript"
		src="${resource(dir:'js',file:'SmoothScroll.js')}"></script>

	<script type="text/javascript"
		src="${resource(dir:'js',file:'jquery.isotope.js')}"></script>

	<script type="text/javascript"
		src="${resource(dir:'js',file:'owl.carousel.js')}"></script>

	<script type="text/javascript"
		src="${resource(dir:'js',file:'main.js')}"></script>
		
	<script type="text/javascript"
		src="${resource(dir:'js',file:'moment.min.js')}"></script>
		
	<script type="text/javascript"
		src="${resource(dir:'js',file:'moment-with-locales.js')}"></script>

</body>
</html>
