package todeolho

class Denuncia {

	static marshalling={

		shouldOutputVersion false
		shouldOutputClass false
		attribute 'descricao'
		ignore 'curtidaToDeOlho','curtidaToNaMidia','dateCreated','imagem'
		deep 'categoria','localizacao','perfil','comentarios'
	}

	String descricao
	Categoria categoria
	Localizacao localizacao
	byte[] imagem
	boolean resolvida
	boolean banida
	Integer totalToDeOlho
	Integer totalToNaMidia
	Perfil perfil
	Date dateCreated
	Date lastUpdated
	
	static hasMany = [comentarios:Comentario,curtidaToDeOlho:ToDeOlho,curtidaToNaMidia:ToNaMidia]
	
	static constraints = {
		descricao nullable:false, blank:false
		imagem nullable:true, maxSize:5000000
		totalToDeOlho min: new Integer(0)
		totalToNaMidia min: new Integer(0)
	}
	
	static mapping = {
		descricao type: 'text'
		localizacao cascade: "delete"
		comentarios cascade: "delete"
	}
	
	String toString() {"${descricao}"}
}
