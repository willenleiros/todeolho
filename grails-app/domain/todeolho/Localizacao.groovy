package todeolho

class Localizacao {

	static marshalling={

		shouldOutputVersion false
		shouldOutputClass false

	}

	String txtEndereco
	Double txtLatitude
	Double txtLongitude
	
	static belongsTo = [Localizacao]

	static constraints = {
		txtEndereco nullable:false, blank:false
		txtLatitude nullable:false, blank:false
		txtLongitude nullable:false, blank:false
	}
}
