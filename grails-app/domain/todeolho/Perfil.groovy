package todeolho

class Perfil {

	static marshalling={

		shouldOutputVersion false
		shouldOutputClass false
		attribute 'nome'
		ignore 'imagem','curtidaToNaMidia','denuncias','comentarios',
				'cpf_cnpj','user','dataNascimento','email','status'
		deep 'curtidaToDeOlho'
	}

	String nome
	String email
	String cpf_cnpj
	Date dataNascimento
	byte[] imagem
	User user
	
	static hasMany = [denuncias:Denuncia,curtidaToDeOlho:ToDeOlho,curtidaToNaMidia:ToNaMidia,comentarios:Comentario]

	static constraints = {
		nome nullable:true, blank:false
		email nullable:true, blank:false
		cpf_cnpj nullable:true, blanck: false
		dataNascimento nullable:true, blank:false
		imagem nullable:true, maxSize:5000000
	}
	
	static mapping = {
		user cascade: "delete"
		denuncias cascade: "delete"
		curtidaToDeOlho cascade: "delete"
		curtidaToNaMidia cascade: "delete"
	}
}

