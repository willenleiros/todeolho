package todeolho

class Categoria {

	static marshalling={

		shouldOutputVersion false
		shouldOutputClass false
		attribute 'nome'
		ignore 'denuncias'

	}

	String nome
	String descricao

	static hasMany = [denuncias:Denuncia]

	static constraints = {
	}

	String toString() { "${nome} - ${descricao}"}
}
