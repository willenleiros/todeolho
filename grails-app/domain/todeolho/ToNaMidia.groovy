package todeolho

class ToNaMidia {
	static marshalling={

		shouldOutputIdentifier false
		shouldOutputVersion false
		shouldOutputClass false
		ignore 'denuncia'

	}

	static belongsTo = [denuncia:Denuncia,perfil:Perfil]

	static constraints = {
	}
}
