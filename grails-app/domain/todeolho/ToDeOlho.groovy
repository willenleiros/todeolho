package todeolho

class ToDeOlho {
	static marshalling={
		
		shouldOutputIdentifier false
		shouldOutputVersion false
		shouldOutputClass false
		ignore 'perfil'

	}
	
	static belongsTo = [denuncia:Denuncia,perfil:Perfil]

	static constraints = {
	}
}
