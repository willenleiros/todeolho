package todeolho

class Comentario {

	static marshalling={

		shouldOutputVersion false
		shouldOutputClass false
		ignore 'denuncia','dateCreated','lastUpdated','melhorComentario'
		deep 'perfil'

	}

	String texto
	Date dateCreated
	Date lastUpdated
	boolean melhorComentario

	static belongsTo = [denuncia:Denuncia,perfil:Perfil]

	static constraints = {
		texto nullable:false, blank:false
	}
}
