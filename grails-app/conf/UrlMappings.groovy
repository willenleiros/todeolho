class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
		
		/*######Recurso RESTFULL para User API#####*/
		"/api/user"(resources:'userApi')
		
		/*######Recurso RESTFULL para Denuncias API#####*/
		"/api/denuncia"(resources:'denunciaAPI')
		//Recursos para Denuncias mapeamento da imagem
		"/api/denuncia/$id/imagem"(controller: "denunciaAPI", action: "imagem")
		//Recursos para Denuncias listagem pessoais
		"/api/denuncia/perfil/$id"(controller: "denunciaAPI", action: "pessoais")
		
		/*######Recursos para perfil API#######*/
		"/api/perfil"(resources:'perfilAPI')
		//Recursos para Perfil mapeamento da imagem
		"/api/perfil/$id/imagem"(controller: "perfilAPI", action: "imagem")
		"/api/perfil/user"(controller: "perfilAPI", action: "perfilPorUser")
		
		
		/*######Recursos para Categoria API#####*/
		"/api/categoria"(resources:'categoriaAPI')
		
		/*######Recursos para Todeolho API#####*/
		"/api/todeolho"(resources:'toDeOlhoAPI')
		"/api/todeolho/curtir"(controller: "toDeOlhoAPI", action: "curtir")
		"/api/todeolho/denuncia/$id"(controller: "toDeOlhoAPI", action: "totalCurtidas")
		
		/*#####Recursos para Localizacao geocodereverse######*/
		"/api/getGeocodeReverse/latitude/$latitude/longitude/$longitude"(controller: "LocalizacaoAPI", action: "geocodereverse")
		/*#####Recursos para Localizacao#######*/
		"/api/localizacao"(controller: "LocalizacaoAPI", action: "getLocalizacoes")
		/*#####Recursos para Localizacao mapeamento do marcador#######*/
		"/api/localizacao/icontodeolho"(controller: "LocalizacaoAPI", action: "getIconToDeOlho")
		
		/*######Recursos para Usuario API#######*/
		"/api/usuario"(resources:'usuarioAPI')
		
		
		//Mapeamento de views
        "/"(view:"/index")
        "500"(view:'/pagina500')
		"404"(view:'/pagina404')
		"401"(view:'/pagina401')
		"/api"(view:"api")
		
		//Recursos REST para complementar web
		"/denuncia/pessoais"(controller: "denuncia", action: "pessoais")
		
		
		//Recursos REST para outros sistemas
		"/api/denuncia/$id"(controller: "API", action: "obterDenunciaPorCodigo")
		
		"/api/denuncia"(controller: "API", action: "obterDenunciasRecentes")
		
		"/api/anonymous/denuncia/mapaDeCalor"(controller: "API", action: "obterMapaDeCalor")
		
		"/api/denuncia/listarRecentes"(controller: "API", action: "obterDenunciasRecentes")
		
		"/api/denuncia/listarResolvidas"(controller: "API", action: "obterDenunciasResolvidas")
		
		"/api/denuncia/listarNaoResolvidas"(controller: "API", action: "obterDenunciasNaoResolvidas")
		
		"/api/denuncia/listarPorCategoria/$descricao"(controller: "API", action: "obterDenunciasPorCategoria")
		
		"/api/denuncia/listarPorBairro/$bairro"(controller: "API", action: "obterDenunciasPorBairro")
		
		"/api/categoria/todas"(controller: "API", action: "obterTodasCategorias")
		
	}
}
