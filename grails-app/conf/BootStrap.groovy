import todeolho.Role
import todeolho.User
import todeolho.UserRole
import todeolho.Perfil

class BootStrap {

    def init = { servletContext ->
		
		//Cria os papeis iniciais
		def adminRole = new Role('ROLE_ADMIN').save()
		def cidadaoRole = new Role('ROLE_CIDADAO').save()
		def midiaRole = new Role('ROLE_MIDIA').save()
		def prefeituraRole = new Role('ROLE_PREFEITURA').save()
		
  
		//Cria os usuarios inicias
		def adminUser = new User('admin', 'admintodeolhonatal').save()
  
		Perfil perfil = new Perfil(user:adminUser)
		
		perfil.save flush:true
		
		//Define os papeis para os usuarios iniciais
		UserRole.create(adminUser, adminRole, true)
		
    }
    def destroy = {
    }
}
