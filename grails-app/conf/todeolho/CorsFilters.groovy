package todeolho

class CorsFilters {

	def filters = {
		all(controller:'*', action:'*') {
			before = {
				response.setHeader("Access-Control-Allow-Origin", "*");
				response.setHeader("Access-Control-Allow-Credentials", "true");
				response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS");
				response.setHeader("Access-Control-Allow-Headers", "origin, authorization, accept, content-type, x-requested-with");
				response.setHeader("Access-Control-Expose-Headers", "x-requested-with");
				response.setHeader("Access-Control-Max-Age","3600");

			}
		}
	}
}
