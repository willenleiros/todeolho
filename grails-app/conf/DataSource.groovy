dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = "todeolho.ImprovedMySQLDialect"
	dialect = "todeolho.MySQLUTF8InnoDBDialect"
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update"
            url = "jdbc:mysql://localhost/todeolho"
            username = "root"
            password = "ninocafe"
			
			dbProperties {
				autoReconnect=true
				jdbcCompliantTruncation=true
			}
			
			properties {
				maxIdle = 50
				maxActive = 50
				minIdle = 10
				testOnBorrow = true
			}
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:mysql://localhost/todeolhoTeste"
            username = "root"
            password = "ninocafe"
			
			dbProperties {
				autoReconnect=true
				jdbcCompliantTruncation=true
			}
			
			properties {
				maxIdle = 50
				maxActive = 50
				minIdle = 10
				testOnBorrow = true
			}
        }
    }
    production {
        dataSource {
			
            dbCreate = "update"
            url = "jdbc:mysql://192.168.33.10/todeolho"
            username = "root"
            password = "ninocafe"
			
			dbProperties {
				autoReconnect=true
				jdbcCompliantTruncation=true
			}
			
			properties {
				maxIdle = 50
			    maxActive = 50
			    minIdle = 10
				testOnBorrow = true
				
				//validationQuery = "SELECT 1"
				//testWhileIdle = true
				//maxAge = 10 * 60000
				
			}   
        }
    }
}