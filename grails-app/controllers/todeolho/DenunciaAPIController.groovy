package todeolho

import grails.converters.JSON
import grails.transaction.*
import static org.springframework.http.HttpStatus.*
import static org.springframework.http.HttpMethod.*
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN','ROLE_CIDADAO','ROLE_MIDIA','ROLE_PREFEITURA'])
class DenunciaAPIController {

	static allowedMethods = [save: "POST", saveImagem: "POST", update: "PUT", delete: "DELETE", imagem:"GET"]
	
	def hdImageService
	
	def denunciaService
	
	def imagem(){
		
		println("Id: "+params.id)
		
		if(params.id == null || params.id == "{{denuncia.id}}"){
			render status: 404
			return
		}
		
		Denuncia denunciaInstance = Denuncia.get(params.id as long)
		
		if(params.id){
			
			if(denunciaInstance.imagem){
				println("Entrou busca da imagem perfil no banco")
				response.outputStream << denunciaInstance.imagem
			}
			else{
				println("Entrou busca da imagem perfil no projeto")
				def webrootDir = servletContext.getRealPath("/") //app directory
				//def webrootDir = servletContext.getContextPath() //app directory
				println(webrootDir)
				File fileDest = new File(webrootDir,"images/logo/camera.jpg")
				response.outputStream << fileDest.getBytes()
			}	
		}
	}
	
	def pessoais(Integer posicao) {
		
		if(params.id == null || params.id == ""){
			render status: 404
			return
		}
		
		if(posicao == null || posicao == ""){
			posicao = 0
		}
		
		def usr = Usuario.get(params.id)
		
		def prf = Perfil.findByUsuario(usr)
		
		println("Id do perfil: "+prf.id)
		
		def denuncias = Denuncia.withCriteria {
			perfil{
				idEq(prf.id as long)
			}
			maxResults(8)
			firstResult(posicao)
			order("dateCreated", "desc")
		
		}
		
		render denuncias as JSON
	}

	def index(Integer posicao) {

		if(posicao == null || posicao == ""){
			posicao = 0
		}
		
		def denuncias = Denuncia.withCriteria {
			
			println params

			maxResults(8)
			firstResult(posicao)
			order("dateCreated", "desc")

		}

		render denuncias as JSON
	}

	def show(Denuncia denuncia) {

		if(denuncia == null) {
			render status: 404
		}
		else {
			render denuncia as JSON
		}
	}

	@Transactional
	def save(Denuncia denuncia) {
		
		ByteArrayInputStream bis = new ByteArrayInputStream(denuncia.getImagem())
		
		byte[] imagem = hdImageService.scale(bis, 660, 660)

		
		denunciaService.save(denuncia, denuncia?.getLocalizacao(), denuncia?.getPerfil(), imagem)
		
		render  status: 200
		

	}

	@Transactional
	def update(Denuncia denuncia) {
		if (denuncia == null) {
			render status: NOT_FOUND
		}

		if (denuncia.hasErrors()) {
			respond denuncia.errors, view:'edit'
			return
		}

		denuncia.save flush:true

		withFormat {
			html {
				flash.message = message(code: 'default.updated.message', args: [message(code: 'denuncia.label', default: 'Denuncia'), denuncia.id])
				redirect denuncia
			}
			'*' { render status: OK }
		}
	}

	@Transactional
	def delete(Denuncia denuncia){

		if (denuncia == null) {
			render status: 404
			return
		}
		try{
			denuncia.delete flush:true
		}catch(Exception e){
			render status: 406
			return
		}
		
		withFormat {
			html {
				flash.message = message(code: 'default.deleted.message', args: [message(code: 'Denuncia.label', default: 'Denuncia'), denuncia.id])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: 200 }
		}
	}
}
