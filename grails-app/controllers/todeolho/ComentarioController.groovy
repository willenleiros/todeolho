package todeolho



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN','ROLE_CIDADAO','ROLE_MIDIA','ROLE_PREFEITURA'])
class ComentarioController {
	
	def springSecurityService
	
	def comentarioService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Comentario.list(params), model:[comentarioInstanceCount: Comentario.count()]
    }

    def show(Comentario comentarioInstance) {
        respond comentarioInstance
    }

    def create(Denuncia denunciaInstance) {
		Comentario com = new Comentario(denuncia:denunciaInstance)
        respond com
    }

    @Transactional
    def save(Comentario comentarioInstance) {
		
        if (comentarioInstance == null) {
            notFound()
            return
        }
		
		User usuario = springSecurityService.currentUser
		
		Perfil perfil = Perfil.findByUser(usuario)
		
		comentarioService.salvar(comentarioInstance, perfil)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'comentario.label', default: 'Comentario'), comentarioInstance.id])
				redirect(url:'/denuncia/show/'+comentarioInstance.denuncia.id)
            }
            '*' { respond comentarioInstance, [status: CREATED] }
        }
    }

    def edit(Comentario comentarioInstance) {
        respond comentarioInstance
    }

    @Transactional
    def update(Comentario comentarioInstance) {
        if (comentarioInstance == null) {
            notFound()
            return
        }

        if (comentarioInstance.hasErrors()) {
            respond comentarioInstance.errors, view:'edit'
            return
        }

        comentarioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Comentario.label', default: 'Comentario'), comentarioInstance.id])
				redirect(url:'/denuncia/show/'+comentarioInstance.denuncia.id)
            }
            '*'{ respond comentarioInstance, [status: OK] }
        }
    }

    @Transactional
    def remove(Comentario comentarioInstance) {

        if (comentarioInstance == null) {
            notFound()
            return
        }

        comentarioInstance.delete flush:true

		redirect(url:'/denuncia/show/'+comentarioInstance.denuncia.id)
        
    }
	
	@Transactional
	def finalizarSatisfeito(Comentario comentarioInstance) {
		println("Executou metodo finalizar")
		Denuncia denunciaInstance = Denuncia.get(comentarioInstance.denuncia.id)
		println("Denuncia id: "+denunciaInstance.id)
		
		denunciaInstance.setResolvida(true)
		denunciaInstance.save(flush:true)
		
		println("Denuncia marcada como resolvida")
		
		comentarioInstance.setMelhorComentario(true)
		comentarioInstance.save(flush:true)
		
		println("Comentario marcado como melhor")
		
		redirect(url:'/denuncia/show/'+denunciaInstance.id)
		println("Finalizou o metodo")
	}
	
	@Transactional
	def finalizarInsatisfeito(Denuncia denunciaInstance) {
		println("Executou metodo finalizar insatisfeito")
		
	}

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'comentario.label', default: 'Comentario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
