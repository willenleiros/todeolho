package todeolho

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON

@Secured(['ROLE_ADMIN','ROLE_CIDADAO','ROLE_MIDIA','ROLE_PREFEITURA'])
class UserApiController {
	
	def userService
	def springSecurityService
	def messageSource
	
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() { }
	
	@Transactional
	@Secured('permitAll')
	def save(User usuario) {
		
		println params
		
		def resposta = [:]
		
		if (usuario == null) {
			render status : 404
		}
		
		usuario.validate()
		if (usuario.hasErrors()) {
	
			String message = ""
			
			def locale = Locale.getDefault()
			for (fieldErrors in usuario.errors) {
			   for (error in fieldErrors.allErrors) {
				  message = messageSource.getMessage(error, locale)
			   }
			}
			
			resposta.put("status", "error")
			resposta.put("mensagem", message)
			
			render resposta as JSON
			
			return
		}
		
		//Role roleInstance = Role.load(params.tipoPerfil as int)
		
		Role role = Role.load(2)
			
		userService.salvar(usuario, role)
		
		resposta.put("status", "success")
		resposta.put("mensagem", "Usuário cadastrado com sucesso.")
			
		response.status = 201
		
		render resposta as JSON
		
		
	}
	
	@Transactional
	def update(User userInstance) {
		if (userInstance == null) {
			return
		}

		if (userInstance.hasErrors()) {
			respond userInstance.errors, view:'edit'
			return
		}

		userInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
				redirect userInstance
			}
			'*'{ respond userInstance, [status: OK] }
		}
	}
	
	@Transactional
	def delete(User userInstance) {

		if (userInstance == null) {
			notFound()
			return
		}

		userInstance.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}
	
	
}
