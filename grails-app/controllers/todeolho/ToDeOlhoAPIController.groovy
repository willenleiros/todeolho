package todeolho

import grails.converters.JSON
import static org.springframework.http.HttpStatus.*
import static org.springframework.http.HttpMethod.*
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN','ROLE_CIDADAO'])
class ToDeOlhoAPIController {
	
	def toDeOlhoAPIService
	
	static allowedMethods = [curtir: "POST", totalCurtidas: "GET"]
	
	def totalCurtidas(){
		println params
		if(params){
			
			def den = Denuncia.get(params.id)
			response.status = 200
			render den.totalToDeOlho
		}
	}

    def curtir(Denuncia denuncia) {
		
		println (params)
		
		if(denuncia == null) {
			render status: 404
		}
		else {
			
			println (denuncia as JSON)
			
			String retorno = toDeOlhoAPIService.executarCurtida(denuncia)
			
			response.status = 200
			
			render retorno
			
		}
	}
}
