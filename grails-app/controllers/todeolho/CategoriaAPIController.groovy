package todeolho

import grails.converters.JSON
import grails.transaction.*
import static org.springframework.http.HttpStatus.*
import static org.springframework.http.HttpMethod.*
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN','ROLE_CIDADAO','ROLE_MIDIA','ROLE_PREFEITURA'])
class CategoriaAPIController {

    static allowedMethods = [index: "GET"]

	def index() {

		def categorias = Categoria.list()

		render categorias as JSON
	}
}
