package todeolho

import grails.plugins.rest.client.RestBuilder
import grails.converters.JSON
import grails.transaction.*
import static org.springframework.http.HttpStatus.*
import static org.springframework.http.HttpMethod.*

class LocalizacaoAPIController {
	
	static allowedMethods = [geocodereverse:"GET"]

    def geocodereverse() {
		
		def retorno = [:]
		
		println("Entrou no metodo geocodereverse")
		println("Parametros: "+params)
		
		if(params.latitude == null || params.latitude == ""){
			render status: 404
			return
		}
		
		if(params.longitude == null || params.longitude == ""){
			render status: 404
			return
		}
		
		//Contantes API GEOCODIND MAPS
		String PROTOCOLO_MAPS = "https://";
		String API_MAPS = "maps.googleapis.com/maps/api/geocode/";
		String PARAMS_JSON = "json";
		String PARAMS_LATLNG = "latlng";
		String PARAMS_KEY = "key";
		String API_KEY = "AIzaSyAvR3ZtDBVy2GhSCPFfvj61xC5UdOLYr9c";
		
		String url = PROTOCOLO_MAPS + API_MAPS + PARAMS_JSON + "?" + PARAMS_LATLNG + "=" + params.latitude + "," + params.longitude + "&" + PARAMS_KEY + "=" + API_KEY;
				
		def resp = new RestBuilder().get(url)
				
		String body = resp.getBody()
		
		println("Resposta: "+ body)
		println("Resposta status: "+resp.json.status)
		
		if(resp.json.status == "OK"){
			
			retorno.put("placeid", resp.json.results.place_id[0])
			retorno.put("endereco", resp.json.results.formatted_address[0])
			retorno.put("status", "OK")
			println("JSON: "+resp.json.results.formatted_address[0])
			
			render retorno as JSON
			
		}
		else{
			retorno.put("status", resp.json.status)
			render retorno as JSON
		}
	}
	
	def getLocalizacoes(){
		
		def localizacoes = Localizacao.withCriteria {}
		
		render localizacoes as JSON
		
	}
	
	def getIconToDeOlho(){
		println("Entrou busca da imagem perfil no projeto")
		def webrootDir = servletContext.getRealPath("/") //app directory
		//def webrootDir = servletContext.getContextPath() //app directory
		println(webrootDir)
		File fileDest = new File(webrootDir,"images/icones/marcador.png")
		response.outputStream << fileDest.getBytes()
	}
}
