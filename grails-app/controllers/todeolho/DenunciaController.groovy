package todeolho



import static org.springframework.http.HttpStatus.*
import grails.rest.RestfulController;
import grails.transaction.Transactional;
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN','ROLE_CIDADAO','ROLE_MIDIA','ROLE_PREFEITURA'])
class DenunciaController {
	
	def springSecurityService

    static allowedMethods = [save: "POST", delete: "DELETE"]
	
	def hdImageService
	
	def denunciaService
	
	
	def imagem(){
		if(params.id){
			Denuncia denunciaInstance = Denuncia.get(params.id)
			if(denunciaInstance){
				response.outputStream << denunciaInstance.imagem
			}
		}
	}
	
	@Secured(['ROLE_ADMIN','ROLE_CIDADAO'])
	def pessoais(){
		
		println params
		
		User usuario = springSecurityService.currentUser
		
		Perfil perfil = Perfil.findByUser(usuario)
		
		def denuncias = Denuncia.findAllByPerfil(perfil)
		
		respond denuncias
		
	}

    def index(Integer max) {
		
		params.max=Math.min(params.max ? params.int('max') : 8,8)
		params.sort="dateCreated"
		params.order="desc"
		
		respond Denuncia.list(params), model:[denunciaInstanceCount: Denuncia.count()]
    }

    def show(Denuncia denunciaInstance) {
		
		User usuario = springSecurityService.currentUser
		Perfil perfil = Perfil.findByUser(usuario)
		
        respond denunciaInstance, model:[perfilLoggedInUser: perfil]
    }

    def create() {
		
        respond new Denuncia(params)
    }

    @Transactional
    def save(Denuncia denunciaInstance, Localizacao localizacaoInstance) {
		
		User usuario = springSecurityService.currentUser
		Perfil perfil = Perfil.findByUser(usuario)
		
		def temp = params.imagem
		
		if (temp.empty) {
			respond flash.message = "É necessário inserir uma imagem", view:'create'
			return
		}
		
		if(!(temp.getContentType() == "image/jpeg" || temp.getContentType() == "image/png")){
			respond flash.message = "Tipo de arquivo inválido: Aceitado formato PNG e JPG.", view:'create'
			return
		}
		
		byte[] imagem = hdImageService.scale(temp.getInputStream(), 660, 660)
			
		//Verifica se a imagem é maior que 5megabytes
		if (imagem.size() > 5000000){
			respond flash.message = "Tamanho de arquivo inválido: Máximo 5 megabytes.", view:'create'
			return
		}
		
		try{
			
			denunciaService.save(denunciaInstance, localizacaoInstance, perfil, imagem)
			
			flash.message = "Denúncia cadastrada com sucesso"
			redirect action: "pessoais", controller:"denuncia"
			
		}
		catch(Exception e){
			flash.message = e.getMessage()
			redirect action: "create", controller:"denuncia"
			return
		}
    }
	
	@Transactional
	def update(Denuncia denunciaInstance) {
		
		if (denunciaInstance == null) {
			notFound()
			return
		}
		
		if (denunciaInstance.descricao == null) {
			respond denunciaInstance.errors, view:'create'
			return
		}
		
		println("Imagem: "+denunciaInstance.imagem.size())
		
		println("Localizacao parametro: "+denunciaInstance?.localizacao?.id)
		println("Localizacao parametro: "+params.txtEndereco)
		println("Localizacao parametro: "+params.txtLatitude)
		println("Localizacao parametro: "+params.txtLongitude)
		
		Localizacao loc = Localizacao.get(denunciaInstance?.localizacao?.id)
		
		println("Localizacao pesquisada: "+loc.id)
		println("Localizacao pesquisada: "+loc.txtEndereco)
		println("Localizacao pesquisada: "+loc.txtLatitude)
		println("Localizacao pesquisada: "+loc.txtLongitude)
		
		loc.setTxtEndereco(params.txtEndereco)
		loc.setTxtLatitude(params.txtLatitude as Double)
		loc.setTxtLongitude(params.txtLongitude as Double)
		
		println("Localizacao mudada: "+loc.id)
		println("Localizacao mudada: "+loc.txtEndereco)
		println("Localizacao mudada: "+loc.txtLatitude)
		println("Localizacao mudada: "+loc.txtLongitude)
		
		if(loc.merge(flush:true)){
			println("Atualizou a localização.")
		}
		else{
			println("Não Atualizou a localização.")
		}
		
		denunciaInstance.setLocalizacao(loc)
			
		if(denunciaInstance.merge(flush:true)){
			println("Atualizou a denuncia.")
		}

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [message(code: 'denuncia.label', default: 'Denuncia'), denunciaInstance.id])
				redirect denunciaInstance
			}
			'*' { respond denunciaInstance, [status: OK] }
		}
	}

    def edit(Denuncia denunciaInstance) {
		
		User usuario = springSecurityService.currentUser
		Perfil perfil = Perfil.findByUser(usuario)
		
		if(perfil == null){
			respond new Denuncia(), [status : 401] 
			return
		}
		
		if(denunciaInstance == null){
			respond new Denuncia(), [status : 401]
			return
		}

		if(denunciaInstance.perfil == perfil){
			respond denunciaInstance
		}
		else{
			respond new Denuncia(), [status : 401] 
			return
		}
        
    }
	
	/*
	 * Apaga a denuncia pelo link
	 */
	@Transactional
	def apagar(Denuncia denunciaInstance){
		
		if (denunciaInstance == null) {
			notFound()
			return
		}
		
		User usuario = springSecurityService.currentUser
		Perfil perfil = Perfil.findByUser(usuario)

		if(denunciaInstance.perfil == perfil){
			denunciaInstance.delete()
		}
		
		redirect(url:'/denuncia/pessoais')
	}
	
	/*
	 * Apaga a denuncia pelo form
	 */
	@Transactional
	def delete(Denuncia denunciaInstance){
		
		if (denunciaInstance == null) {
			notFound()
			return
		}
		
		User usuario = springSecurityService.currentUser
		Perfil perfil = Perfil.findByUser(usuario)

		if(denunciaInstance.perfil == perfil){
			denunciaInstance.delete()
		}
		
		redirect(url:'/denuncia/pessoais')
	}

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'denuncia.label', default: 'Denuncia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
