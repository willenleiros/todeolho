package todeolho



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN','ROLE_CIDADAO','ROLE_MIDIA','ROLE_PREFEITURA'])
class PerfilController {

	def springSecurityService

	static allowedMethods = [save: "POST", delete: "DELETE"]

	def imagemUserLogado(){

		User usuario = springSecurityService.currentUser

		Perfil perfil = Perfil.findByUser(usuario)

		if(perfil){

			if(perfil.imagem){
				println("Entrou busca da imagem perfil no banco")
				response.outputStream << perfil.imagem
			}
			else{
				println("Entrou busca da imagem perfil no projeto")
				def webrootDir = servletContext.getRealPath("/") //app directory
				//def webrootDir = servletContext.getContextPath() //app directory
				println(webrootDir)
				File fileDest = new File(webrootDir,"images/team/blank.jpg")
				response.outputStream << fileDest.getBytes()
			}
		}
	}

	def imagem(){

		if(params.id == null){
			notFound()
		}

		Perfil perfil = Perfil.get(params.id as int)

		if(perfil){

			if(perfil.imagem){
				println("Entrou busca da imagem perfil no banco")
				response.outputStream << perfil.imagem
			}
			else{
				println("Entrou busca da imagem perfil no projeto")
				def webrootDir = servletContext.getRealPath("/") //app directory
				//def webrootDir = servletContext.getContextPath() //app directory
				println(webrootDir)
				File fileDest = new File(webrootDir,"images/team/blank.jpg")
				response.outputStream << fileDest.getBytes()
			}
		}
	}

	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		respond Perfil.list(params), model:[perfilInstanceCount: Perfil.count()]
	}

	def show(Perfil perfilInstance) {
		respond perfilInstance
	}

	def create() {
		respond new Perfil(params)
	}

	@Transactional
	def save(Perfil perfilInstance) {

		def usuarioInstance = springSecurityService.currentUser

		if (usuarioInstance == null) {
			notFound()
			return
		}

		if (perfilInstance == null) {
			notFound()
			return
		}

		if (perfilInstance.hasErrors()) {
			respond perfilInstance.errors, view:'create'
			return
		}

		perfilInstance.setUser(usuarioInstance)

		perfilInstance.save()

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.created.message', args: [message(code: 'perfil.label', default: 'Perfil'), perfilInstance.id])
				redirect perfilInstance
			}
			'*' { respond perfilInstance, [status: CREATED] }
		}
	}

	def edit() {

		def usuarioInstance = springSecurityService.currentUser

		Perfil perfil = Perfil.findByUser(usuarioInstance)

		respond perfil
	}

	@Transactional
	def update(Perfil perfilInstance) {

		println params
		println perfilInstance

		if (perfilInstance == null) {
			notFound()
			return
		}

		if (perfilInstance.hasErrors()) {
			respond perfilInstance.errors, view:'edit'
			return
		}

		def usuarioInstance = springSecurityService.currentUser

		perfilInstance.setUser(usuarioInstance)

		if(perfilInstance.save(flush:true)){
			println("Atualizou o perfil.")
		}

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [message(code: 'Perfil.label', default: 'Perfil'), perfilInstance.id])
				//redirect perfilInstance
				redirect(controller: "denuncia", action: "index")
			}
			'*'{ respond perfilInstance, [status: OK] }
		}
	}

	@Transactional
	def delete(Perfil perfilInstance) {

		if (perfilInstance == null) {
			notFound()
			return
		}

		// User usuario = User.get(perfilInstance.usuario.id)

		// perfilInstance.delete()
		// usuario.delete()

		// redirect(url:'/usuario/login/')

	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [message(code: 'perfil.label', default: 'Perfil'), params.id])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
}
