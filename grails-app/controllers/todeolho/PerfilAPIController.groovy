package todeolho

import grails.converters.JSON
import grails.transaction.*
import static org.springframework.http.HttpStatus.*
import static org.springframework.http.HttpMethod.*
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN','ROLE_CIDADAO','ROLE_MIDIA','ROLE_PREFEITURA'])
class PerfilAPIController {

	static allowedMethods = [imagem:"GET", perfilPorUser:"POST"]

	def imagem() {
		
		if(params.id == null || params.id == "{{perfil.id}}" || params.id == "{{denuncia.perfil.id}}"){
			render status: 404
			return
		}

		Perfil perfil = Perfil.get(params.id as long)
		
		if(params.id){
			if(perfil.imagem){
				println("Entrou busca da imagem perfil no banco")
				response.outputStream << perfil.imagem
			}
			else{
				println("Entrou busca da imagem perfil no projeto")
				def webrootDir = servletContext.getRealPath("/") //app directory
				//def webrootDir = servletContext.getContextPath() //app directory
				println(webrootDir)
				File fileDest = new File(webrootDir,"images/team/blank.jpg")
				response.outputStream << fileDest.getBytes()
			}
		}
	}

	def index() {

		def perfies = Perfil.withCriteria {

			maxResults(8)

		}

		render perfies as JSON
	}

	def show(Perfil perfil) {

		if(perfil == null) {
			render status: 404
			return
		}
		else {
			render perfil as JSON
		}
	}
	
	def perfilPorUser(){
		
	
		String parametro = request.JSON.getString("username")
		
		
		if(!parametro){
			render status : 404
			return
		}
		
		User usuario = User.findByUsername(parametro)
		
		Perfil perfil = Perfil.findByUser(usuario)
		
		render perfil as JSON
		
	}
}
