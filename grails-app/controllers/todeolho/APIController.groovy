package todeolho

import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN','ROLE_CIDADAO','ROLE_MIDIA','ROLE_PREFEITURA'])
class APIController {

	def index() { }

	//Lista uma denuncia por código
	def obterDenunciaPorCodigo(){

		if(params?.id && Denuncia.exists(params?.id)) {

			Denuncia den = Denuncia.get(params.id)

			def jsonString =
					[
						"descricao":den.descricao,
						"categoria":den.categoria,
						"localizacao":den.localizacao,
						"resolvida":den.resolvida,
						"banida":den.banida,
						"todeolho":den.curtidaToDeOlho.size(),
						"tonamidia":den.curtidaToNaMidia.size(),
						"comentarios":den.comentarios
					]
			render jsonString as JSON
		}
		else{
			def erro = ["Erro":"Dados não encontrado"]
			render erro as JSON
		}
	}
	
	//Listar as ultimas 8 denuncias cadastradas
	def obterMapaDeCalor(){

		def localizacoes = Localizacao.withCriteria {

			maxResults(1000)

		}

		render localizacoes as JSON
	}

	//Listar as ultimas 8 denuncias cadastradas
	def obterDenunciasRecentes(){

		def denuncias = Denuncia.withCriteria {

			maxResults(8)
			order("dateCreated", "desc")

		}

		render denuncias as JSON
	}

	//Lista todas as denuncias resolvidas
	def obterDenunciasResolvidas(){

		List den = Denuncia.findAllResolvida()

		render den as JSON

	}

	//Lista todas as denuncias não resolvidas
	def obterDenunciasNaoResolvidas(){

		List den = Denuncia.findAllNotResolvida()

		render den as JSON

	}

	//Lista todas as denuncias de uma categoria
	def obterDenunciasPorCategoria(){

		if(params?.descricao) {

			println(params.descricao)

			List denuncias = Denuncia.withCriteria {

				categoria{
					like("nome", "%${params.descricao}%")
				}

				maxResults(10)
				//order("data", "desc")
			}

			render denuncias as JSON
		}
		else{
			def erro = ["Erro":"Dados não encontrado"]
			render erro as JSON
		}

	}

	//Listar as ultimas 6 denuncias cadastradas
	def obterDenunciasPorBairro(){

		if(params?.bairro) {

			println(params.bairro)

			List denuncias = Denuncia.withCriteria {

				localizacao{
					like("txtEndereco", "%${params.bairro}%")
				}

				maxResults(10)
				//order("data", "desc")
			}

			render denuncias as JSON
		}
		else{
			def erro = ["Erro":"Dados não encontrado"]
			render erro as JSON
		}
	}

	//Obter todas as categorias cadastradas
	def obterTodasCategorias() {

		List categoria = Categoria.list(params)

		render categoria as JSON
	}
}
