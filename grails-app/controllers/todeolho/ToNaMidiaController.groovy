package todeolho



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_MIDIA'])
class ToNaMidiaController {
	
	def springSecurityService

	@Transactional
	def curtidaToNaMidia(Denuncia denunciaInstance){
		
		User usuario = springSecurityService.currentUser
		Perfil perfil = Perfil.findByUser(usuario)
		
		if(denunciaInstance) {
			Denuncia den = Denuncia.read(denunciaInstance.id)
			ToNaMidia toNaMidia = ToNaMidia.findByDenunciaAndPerfil(den,perfil)
			
			if(toNaMidia){
				toNaMidia.delete()
				den.setTotalToNaMidia(den.totalToNaMidia-1)
				den.save(flush:true)
				println("Deletou toNaMidia")
				redirect(url:'/denuncia/show/'+den.id)
			}
			else{
				toNaMidia = new ToNaMidia(denuncia:den,perfil:perfil)
				toNaMidia.save(flush:true)
				den.setTotalToNaMidia(den.totalToNaMidia+1)
				den.save(flush:true)
				println("Salvou toNaMidia")
				redirect(url:'/denuncia/show/'+den.id)
			}
			
		}
		else{
			render status: NOT_FOUND
		}
	}

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'toNaMidia.label', default: 'ToNaMidia'), params.id])
                redirect controller: "denuncia", action: "index", id: "${denunciaInstance.id}"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
