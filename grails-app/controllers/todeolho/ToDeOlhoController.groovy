package todeolho



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_CIDADAO'])
class ToDeOlhoController {
	
	def springSecurityService
	
	@Transactional
	def curtidaToDeOlho(Denuncia denunciaInstance){
		
		User usuario = springSecurityService.currentUser
		Perfil perfil = Perfil.findByUser(usuario)
		
		if(denunciaInstance) {
			Denuncia den = Denuncia.read(denunciaInstance.id)
			ToDeOlho toDeOlho = ToDeOlho.findByDenunciaAndPerfil(den,perfil)
			
			if(toDeOlho){
				toDeOlho.delete()
				den.setTotalToDeOlho(den.totalToDeOlho-1)
				den.save(flush:true)
				println("Deletou toDeOlho")
				redirect(url:'/denuncia/show/'+den.id)
			}
			else{
				toDeOlho = new ToDeOlho(denuncia:den,perfil:perfil)
				toDeOlho.save(flush:true)
				den.setTotalToDeOlho(den.totalToDeOlho+1)
				den.save(flush:true)
				println("Salvou toDeOlho")
				redirect(url:'/denuncia/show/'+den.id)
			}
			
		}
		else{
			render status: NOT_FOUND
		}
	}

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'toNaMidia.label', default: 'ToNaMidia'), params.id])
                redirect controller: "denuncia", action: "index", id: "${denunciaInstance.id}"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
