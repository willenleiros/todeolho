package todeolho



import static org.springframework.http.HttpStatus.*
import grails.rest.RestfulController;
import grails.transaction.Transactional
import grails.converters.*

@Transactional(readOnly = true)
class LocalizacaoController extends RestfulController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	LocalizacaoController(){
		super(Localizacao)
	}
	
	def showLocalizacaoAPI(){
		
		if(params.id && Denuncia.exists(params.id)) {
			def p = Localizacao.get(params.id)
			render p as JSON
		}
		else{
			def erro = ["Erro":"Dados não encontrado"]
			render erro as JSON
		}
	}
	
	def indexLocalizacaoAPI(){
		
		def all = Localizacao.list()
		render all as JSON
		
	}

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Localizacao.list(params), model:[localizacaoInstanceCount: Localizacao.count()]
    }

    def show(Localizacao localizacaoInstance) {
        respond localizacaoInstance
    }

    def create() {
        respond new Localizacao(params)
    }

    @Transactional
    def save(Localizacao localizacaoInstance) {
        if (localizacaoInstance == null) {
            notFound()
            return
        }

        if (localizacaoInstance.hasErrors()) {
            respond localizacaoInstance.errors, view:'create'
            return
        }

        localizacaoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'localizacao.label', default: 'Localizacao'), localizacaoInstance.id])
                redirect localizacaoInstance
            }
            '*' { respond localizacaoInstance, [status: CREATED] }
        }
    }

    def edit(Localizacao localizacaoInstance) {
        respond localizacaoInstance
    }

    @Transactional
    def update(Localizacao localizacaoInstance) {
        if (localizacaoInstance == null) {
            notFound()
            return
        }

        if (localizacaoInstance.hasErrors()) {
            respond localizacaoInstance.errors, view:'edit'
            return
        }

        localizacaoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Localizacao.label', default: 'Localizacao'), localizacaoInstance.id])
                redirect localizacaoInstance
            }
            '*'{ respond localizacaoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Localizacao localizacaoInstance) {

        if (localizacaoInstance == null) {
            notFound()
            return
        }

        localizacaoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Localizacao.label', default: 'Localizacao'), localizacaoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'localizacao.label', default: 'Localizacao'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
