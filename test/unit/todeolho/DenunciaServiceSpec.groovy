package todeolho

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
import org.springframework.mock.web.MockMultipartFile


@TestFor(DenunciaService)
@Mock(Denuncia)
class DenunciaServiceSpec extends Specification {

	void "Verificando sanidade do sistema com localizacao nulo"() {
		
		mockDomain Localizacao
		mockDomain Perfil
		
		when: "Teste: Localizacao nula"
			
			Localizacao localizacao = null
			
			Perfil perfil = new Perfil(id:1)
			
			Denuncia denuncia = new Denuncia()
			denuncia.setBanida(false)
			denuncia.setDescricao("Alguma coisa")
			denuncia.setLocalizacao(null)
			denuncia.setCategoria(new Categoria(id:1))
			denuncia.setResolvida(false)
			denuncia.setTotalToDeOlho(0)
			denuncia.setTotalToNaMidia(0)
			
			byte[] imagem = new byte[1000]
		
		then: "Então"
			try{
				service.saveAPI(denuncia,localizacao,perfil,imagem)
			}
			catch(RuntimeException e){
				assert e.message == "localizacao nulo"
			}
			
		
	}
	
//	void "Verificando sanidade do sistema com atributos de localizacao invalidos"() {
//		
//		mockDomain Localizacao
//		mockDomain Perfil
//		
//		when: "Teste: localizacao com atributos nulos"
//			
//			Localizacao localizacao = new Localizacao()
//			localizacao.setTxtEndereco(null)
//			localizacao.setTxtLatitude(null)
//			localizacao.setTxtLongitude(null)
//			
//			Perfil perfil = new Perfil(id:1)
//			
//			Denuncia denuncia = new Denuncia()
//			denuncia.setBanida(false)
//			denuncia.setDescricao("Alguma coisa")
//			denuncia.setLocalizacao(null)
//			denuncia.setCategoria(new Categoria(id:1))
//			denuncia.setResolvida(false)
//			denuncia.setTotalToDeOlho(0)
//			denuncia.setTotalToNaMidia(0)
//			
//			byte[] imagem = new byte[1000]
//			
//		
//		then: "Então"
//			try{
//				service.saveAPI(denuncia,localizacao,perfil,imagem)
//			}
//			catch(RuntimeException e){
//				assert e.message == "denuncia nulo"
//			}
//		
//	}
	
	void "Verificando sanidade do sistema com denuncia nula"() {
		
		mockDomain Localizacao
		mockDomain Perfil
		
		when: "Teste: denuncia nula"
		
			Localizacao localizacao = new Localizacao()
			localizacao.setTxtEndereco("teste endereco")
			localizacao.setTxtLatitude(5.093839)
			localizacao.setTxtLongitude(-35.76386)
			
			Perfil perfil = new Perfil(id:1)
			
			byte[] imagem = new byte[1000]
		
		then: "Então"
			try{
				service.saveAPI(null,localizacao,perfil,imagem)
			}
			catch(RuntimeException e){
				assert e.message == "denuncia nulo"
			}
		
	}
	
	void "Verificando sanidade do sistema com imagem nula"() {
		
		mockDomain Localizacao
		mockDomain Perfil
		
		when: "Teste: imagem nula"
		
			Localizacao localizacao = new Localizacao()
			localizacao.setTxtEndereco("teste endereco")
			localizacao.setTxtLatitude(5.093839)
			localizacao.setTxtLongitude(-35.76386)
			
			Perfil perfil = new Perfil(id:1)
			
			Denuncia denuncia = new Denuncia()
			denuncia.setBanida(false)
			denuncia.setDescricao("Alguma coisa")
			denuncia.setLocalizacao(null)
			denuncia.setCategoria(new Categoria(id:1))
			denuncia.setResolvida(false)
			denuncia.setTotalToDeOlho(0)
			denuncia.setTotalToNaMidia(0)
		
		then: "Então"
			
			try{
				service.saveAPI(denuncia,localizacao,perfil,null)
			}
			catch(RuntimeException e){
				assert e.message == "imagem nulo"
			}
		
	}
	
	void "Verificando sanidade do sistema com atributos de denuncia validos"() {
		
		mockDomain Localizacao
		mockDomain Perfil
		
		given: "Criando Objetos"
		
			Localizacao localizacao = new Localizacao()
			localizacao.setTxtEndereco("teste endereco")
			localizacao.setTxtLatitude(5.093839)
			localizacao.setTxtLongitude(-35.76386)
			
			Perfil perfil = new Perfil(id:1)
			
			Denuncia denuncia = new Denuncia()
			denuncia.setBanida(false)
			denuncia.setDescricao("Alguma coisa")
			denuncia.setLocalizacao(null)
			denuncia.setCategoria(new Categoria(id:1))
			denuncia.setResolvida(false)
			denuncia.setTotalToDeOlho(0)
			denuncia.setTotalToNaMidia(0)
			
			byte[] imagem = new byte[1000]
		
		when: "Quando"
			
			denuncia.validate() == true
			
		
		then: "Então"
		
			service.saveAPI(denuncia,localizacao,perfil,imagem)
		
	}
    
}
