package todeolho


import grails.test.mixin.*
import spock.lang.*


@TestFor(DenunciaAPIController)
@Mock(Denuncia)
class DenunciaAPIControllerSpec extends Specification {

	def populateValidParams(params) {
		assert params != null
	}

	void "Testar API: Verifica o salvamento de uma denuncia com parametros nulos (Espera retorno status 406-NOT_ACCEPTABLE)"(){
		
		when:"Cria a denuncia passando com objetos inválidos"
		request.contentType = FORM_CONTENT_TYPE
		request.method = 'POST'
		Denuncia denuncia = new Denuncia()
		denuncia.setBanida(false)
		denuncia.setDescricao(null)
		denuncia.setCategoria(null)
		denuncia.setPerfil(null)
		denuncia.setLocalizacao(null)
		denuncia.setResolvida(false)
		denuncia.setTotalToDeOlho(null)
		denuncia.setTotalToNaMidia(null)
		
		denuncia.validate()
		controller.save(denuncia)
		
		then:"Verifica se o resposta foi http 406 NOT_ACCEPTABLE "
		response.status == 406
	}

	void "Testar API: Verifica salvamento de uma denuncia com parametros válidos (Espera retorno status 201-CREATED)"(){
		
		mockDomain Localizacao
		
		when:"Cria a denuncia com objetos válidos "
		request.contentType = FORM_CONTENT_TYPE
		request.method = 'POST'
		Denuncia denuncia = new Denuncia()
		denuncia.setBanida(false)
		denuncia.setDescricao("Alguma coisa")
		denuncia.setLocalizacao(new Localizacao(id:1))
		denuncia.setCategoria(new Categoria(id:1))
		denuncia.setPerfil(new Perfil(id:1))
		denuncia.setResolvida(false)
		denuncia.setTotalToDeOlho(0)
		denuncia.setTotalToNaMidia(0)

		denuncia.validate()
		controller.save(denuncia)
		
		then:"Verifica se a resposta foi http 201 CREATED"
		response.status == 201
	}

	void "Testar API: Verifica a tentativa de exclusão de uma denuncia nula (recebe retorno status 404-NOT_FOUND)"(){
		when:
		request.contentType = FORM_CONTENT_TYPE
		request.method = 'DELETE'

		controller.delete(null)

		then:"Verifica se a resposta foi http 404 NOT_FOUND"
		response.status == 404

	}

	void "Testar API: Verifica se uma denuncia é excluida corretamente (recebe retorno status 200-OK)"(){
		
		def denuncias = [];

		when:"Criar uma instancia"
		response.reset()
		request.contentType = FORM_CONTENT_TYPE
		request.method = 'POST'
		populateValidParams(params)
		def denuncia = new Denuncia(params)
		denuncia.validate()
		denuncia.save(flush: true)
	
		then:"A denuncia existe"
		response.status == 200

		when:"Passo a instancia para ser deletada"
		response.reset()
		request.contentType = FORM_CONTENT_TYPE
		request.method = 'DELETE'
		controller.delete(denuncia)

		then:"A instancia foi deletada"
		response.status == 200
		

	}
	

}
